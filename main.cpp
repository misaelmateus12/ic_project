#include<bits/stdc++.h>
#include "fnv1a.h"
#include "stemming.cpp"

using namespace std;
hash<string> hasher;
const long long P = 1e9 + 7;

class IndexPairHash
{
public:

    template <typename T1, typename T2>
    std::size_t operator () (const std::pair<T1, T2> &pair) const
    {
        return std::hash<T1> () (pair.first) ^ std::hash<T2> () (pair.second);
    }
};

unordered_map<pair<uint64_t, uint64_t>, uint32_t, IndexPairHash> matrix;

const uint8_t MAX_WORD_LENGTH = 30;
char *GetNextWord (char *text, char *token, bool to_stemming = false)
{
    bool quote_found = false;
    bool hyphen_found = false;

    uint8_t char_index = 0;


    while (*text)
    {
        char c = *text++;

        if (isalpha (c))
        {
            c = static_cast<char>(tolower (c));

            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }
        }
        else if ((c == '\'') && (!quote_found) && (char_index > 0))
        {
            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }

            quote_found = true;
        }
        else if ((c == '-') && (!hyphen_found) && (char_index > 0))
        {
            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }

            hyphen_found = true;
        }
        else if (char_index > 0)
        {
            //
            // Normaliza o índice para evitar que a string passe do limite do buffer.
            //
            char_index = (uint8_t) ((char_index > MAX_WORD_LENGTH - 1) ? MAX_WORD_LENGTH - 1 : char_index);

            //
            // Descarta o apóstrofo ou o hífen se este estiver isolado no final do token.
            //
            // Ex.: 'String' -> String'
            //      'Window' -> Window'
            //
            if ((token[char_index - 1] == '\'') || (token[char_index - 1] == '-'))
            {
                --char_index;
            }

            break;
        } else break;
    }

    char_index = min(char_index, MAX_WORD_LENGTH);
    token[char_index] = '\0';

    if(to_stemming)
        stemming(token);
    return (*text) ? text : nullptr;
}


int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	uint64_t word_hash[5];
    
	char word[50];
	char aux[50];
	
    int i = 0;
	int cnt = 0, t = 0;
	
    
    while(cin >> word){
        if(word[0] == '\0') continue;

        GetNextWord(word, aux);

        if(strlen(aux) <= 2) continue;
        word_hash[i] = fnv1a(aux);
        
        for(int j = 0; j < 3 && j < cnt; j++)
            if(j != i){
                matrix[make_pair(word_hash[i], word_hash[j])]++;
                matrix[make_pair(word_hash[j], word_hash[i])]++;
            }
        
        i = (i + 1) % 3;
        cnt++;
        if(cnt % 2000000 == 0)
            cout << cnt << " " << t << endl;
    }

	ofstream out;
	out.open("result.txt", ofstream::out);
    int x = 0;
    for(auto &u : matrix){
        out << u.first.first << " " << u.first.second << " " << u.second << "\n";
    }

    out.close();
    return 0;   
}

