#include<bits/stdc++.h>
#include "fnv1a.h"
#include "stemming.cpp"

using namespace std;
hash<string> hasher;
const long long P = 1e9 + 7;
unordered_map< long long, int > s;
unordered_map<int, int> s_voc;

const uint8_t MAX_WORD_LENGTH = 30;

char *GetNextWord (char *text, char *token, bool to_stemming = false)
{
    bool quote_found = false;
    bool hyphen_found = false;

    uint8_t char_index = 0;


    while (*text)
    {
        char c = *text++;

        if (isalpha (c))
        {
            c = static_cast<char>(tolower (c));

            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }
        }
        else if ((c == '\'') && (!quote_found) && (char_index > 0))
        {
            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }

            quote_found = true;
        }
        else if ((c == '-') && (!hyphen_found) && (char_index > 0))
        {
            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }

            hyphen_found = true;
        }
        else if (char_index > 0)
        {
            //
            // Normaliza o índice para evitar que a string passe do limite do buffer.
            //
            char_index = (uint8_t) ((char_index > MAX_WORD_LENGTH - 1) ? MAX_WORD_LENGTH - 1 : char_index);

            //
            // Descarta o apóstrofo ou o hífen se este estiver isolado no final do token.
            //
            // Ex.: 'String' -> String'
            //      'Window' -> Window'
            //
            if ((token[char_index - 1] == '\'') || (token[char_index - 1] == '-'))
            {
                --char_index;
            }

            break;
        } else break;
    }

    char_index = min(char_index, MAX_WORD_LENGTH);
    token[char_index] = '\0';

    if(to_stemming)
        stemming(token);
    return (*text) ? text : nullptr;
}


int main(){
    ios::sync_with_stdio(0);
    cin.tie(0);
    int word_hash[5];
    char word[50];
    char aux[50];
    int i = 0;
    int cnt = 0, t = 0;
    
    ofstream out;
    out.open("vocabulary.txt", ofstream::out);
    
    while(cin >> word){
        if(word[0] == '\0') continue;

        GetNextWord(word, aux, true);

        if(strlen(aux) <= 2) continue;
        word_hash[i] = fnv1a(aux);
        
        for(int j = 0; j < 4; j++)
            if(j != i){
                s[word_hash[i] * P + word_hash[j]]++;
                s[word_hash[i] * P + word_hash[j]]++;
            }
        
        auto &u = s_voc[word_hash[i]];
        u++;
        if(u == 5){
            t++;
            out << aux << "\n";
        }
        
        i = (i + 1) % 4;
        cnt++;
        if(cnt % 2000000 == 0)
            cout << cnt << " " << t << endl;
            
    }
    int k = 1;
    for(auto &u : s_voc){
        if(u.second > 5)
            u.second = k++;
        else u.second = 0;
    }


    int a = s.size();
    int b = s_voc.size();

    double prob = (a/b)/b;
    cout << a << endl << b << endl << prob << endl;
    out.close();
    return 0;   
}

