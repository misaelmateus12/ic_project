//  Copyright 2013 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

//#include <pthread.h>
#include <omp.h>
#include <map>
#include <vector>
#include <thrust/system/cuda/vector.h>
#include <thrust/execution_policy.h>
#include <thrust/sort.h>
#include <thrust/device_vector.h>

using namespace std;

#ifdef _WIN32

#include <time.h>
#include <sys/timeb.h>
#include <Windows.h>
#define posix_memalign(p, a, s) (((*(p)) = _aligned_malloc((s), (a))), *(p) ?0 :errno)
#define fgetc_unlocked _fgetc_nolock

double gettime() { // granularity about 50 microsecs on my machine
	static LARGE_INTEGER freq, start;
	LARGE_INTEGER count;
	if (!QueryPerformanceCounter(&count))
		// FatalError("QueryPerformanceCounter");
		fprintf(stderr, "QueryPerformanceCounter");
	if (!freq.QuadPart) { // one time initialization
		if (!QueryPerformanceFrequency(&freq))
			//FatalError("QueryPerformanceFrequency");
			fprintf(stderr, "QueryPerformanceCounter");
		start = count;
	}
	return (double)(count.QuadPart - start.QuadPart) / freq.QuadPart;
}


#else

#include <sys/time.h>

double gettime() { // returns 0 seconds first time called
	static struct timeval t0;
	struct timeval tv;
	gettimeofday(&tv, 0);
	if (!t0.tv_sec)
		t0 = tv;
	return tv.tv_sec - t0.tv_sec + (tv.tv_usec - t0.tv_usec) / 1000000.;
}

/*
double gettime() {
timespec ts;
clock_gettime(CLOCK_REALTIME, &ts);
return double(ts.tv_sec) + double(ts.tv_nsec) / 1e9;
}
/**/


#endif

#define ITERS_TO_SYNC 1

#define MAX_STRING 100
#define EXP_TABLE_SIZE 1000
#define MAX_EXP 6
#define MAX_SENTENCE_LENGTH 1024
#define MAX_CODE_LENGTH 40

#define USE_INT_FOR_RAND 0

#if USE_INT_FOR_RAND

#define RAND_MAGIC (unsigned int)1664525 + 1013904223
typedef unsigned int rand_type;

#else

//Mikolov used this one
#define RAND_MAGIC ((unsigned long long)25214903917 + 11)
typedef unsigned long long rand_type;

#endif




#define checkCUDAError(err) {\
	cudaError_t cet = err;\
	if(cudaSuccess != cet){\
		printf("%s %d : %s\n", __FILE__, __LINE__, cudaGetErrorString(cet));\
		exit(0);\
		}\
}

const int vocab_hash_size = 30000000;  // Maximum 30 * 0.7 = 21M words in the vocabulary

typedef float real;                    // Precision of float numbers

struct vocab_word {
	long long cn;
	int *point;
	char *word, *code, codelen;
};

struct Similarity {
	int word;
	real distance;

	__host__ __device__	Similarity() {}
	__host__ __device__ Similarity(int word, real distance) : word(word), distance(distance) {}

	__host__ __device__ bool operator < (const Similarity &sim) const {
		return distance < sim.distance;
	}

	__host__ __device__ bool operator <= (const Similarity &sim) const {
		return distance <= sim.distance;
	}

	__host__ __device__ bool operator >(const Similarity &sim) const {
		return distance > sim.distance;
	}

	__host__ __device__ bool operator >=(const Similarity &sim) const {
		return distance >= sim.distance;
	}
	__host__ __device__ bool operator ==(const Similarity &sim) const {
		return distance == sim.distance;
	}
};


struct GPUVariables {

	real *d_syn0;
	real *d_syn1;
	real *d_syn1neg;
	real *d_expTable;
	

	int *d_table;
	int *d_vocab_codelen;
	int *d_vocab_point;

	char *d_vocab_code;
	
	vector<int*> d_sen;
	vector<int*> sen;

	real *l2norms;//vocab_size
	Similarity *distances;//vocab_size
	int *topks;//vocab_size * K

	rand_type *d_randoms;
	long long *d_word_cns; //vocab_size

};



class cached_allocator
{
public:
	// just allocate bytes
	typedef char value_type;

	cached_allocator() {}

	~cached_allocator()
	{
		// free all allocations when cached_allocator goes out of scope
		free_all();
	}

	char *allocate(std::ptrdiff_t num_bytes)
	{

		char *result = 0;

		// search the cache for a free block
		free_blocks_type::iterator free_block = free_blocks.find(num_bytes);

		/*
		for (free_blocks_type::iterator i = free_blocks.begin();
		i != free_blocks.end();
		++i)
		{
		if (i->first >= num_bytes){
		free_block = i;
		break;
		}
		}/**/

		if (free_block != free_blocks.end())
		{
			//std::cout << "cached_allocator::allocator(): found a hit" << std::endl;

			// get the pointer
			result = free_block->second;

			// erase from the free_blocks map
			free_blocks.erase(free_block);
		}
		else
		{
			// no allocation of the right size exists
			// create a new one with cuda::malloc
			// throw if cuda::malloc can't satisfy the request
			try
			{
				//std::cout << "cached_allocator::allocator(): no free block found; calling cuda::malloc " << num_bytes << std::endl;

				// allocate memory and convert cuda::pointer to raw pointer
				result = thrust::cuda::malloc<char>(num_bytes).get();
			}
			catch (std::runtime_error &e)
			{
				throw;
			}
		}

		// insert the allocated pointer into the allocated_blocks map
		allocated_blocks.insert(std::make_pair(result, num_bytes));

		return result;
	}

	void deallocate(char *ptr, size_t n)
	{
		// erase the allocated block from the allocated blocks map
		allocated_blocks_type::iterator iter = allocated_blocks.find(ptr);
		std::ptrdiff_t num_bytes = iter->second;
		allocated_blocks.erase(iter);

		// insert the block into the free blocks map
		free_blocks.insert(std::make_pair(num_bytes, ptr));
	}

private:
	typedef std::multimap<std::ptrdiff_t, char*> free_blocks_type;
	typedef std::map<char *, std::ptrdiff_t>     allocated_blocks_type;

	free_blocks_type      free_blocks;
	allocated_blocks_type allocated_blocks;

	void free_all()
	{
		std::cout << "cached_allocator::free_all(): cleaning up after ourselves..." << std::endl;
		std::cout << "cached_allocator::size(): number of allocations " << free_blocks.size() + allocated_blocks.size() << std::endl;

		// deallocate all outstanding blocks in both lists
		for (free_blocks_type::iterator i = free_blocks.begin();
			i != free_blocks.end();
			++i)
		{
			// transform the pointer to cuda::pointer before calling cuda::free
			thrust::cuda::free(thrust::cuda::pointer<char>(i->second));
		}

		for (allocated_blocks_type::iterator i = allocated_blocks.begin();
			i != allocated_blocks.end();
			++i)
		{
			// transform the pointer to cuda::pointer before calling cuda::free
			thrust::cuda::free(thrust::cuda::pointer<char>(i->first));
		}
	}
};


int blockSize = 128;
int useWarpApproach = 0; // There is an accuracy problem with the warp approach. Use 0
int syncSyn1Neg = 1; // The correct way is to sync syn1neg too 
int num_gpus = 1;
int threads_per_gpu = 1;
int knnn = 0; //Use K Nearest Neighbors based Negative sampling
int K = 5;
int K_iter_start = 3;
int METHOD = 0;

#include "cbow.cu"

int *vocab_codelen;
char *vocab_code;
int *vocab_point;

char train_file[MAX_STRING], output_file[MAX_STRING];
char save_vocab_file[MAX_STRING], read_vocab_file[MAX_STRING];
struct vocab_word *vocab;
int binary = 0, cbow = 1, debug_mode = 2, window = 5, min_count = 5, num_threads = 1, min_reduce = 1;
int *vocab_hash;
long long vocab_max_size = 1000, vocab_size = 0, layer1_size = 100;
long long train_words = 0, word_count_actual = 0, iter = 5, file_size = 0, classes = 0;
real alpha = 0.025, starting_alpha, sample = 1e-3;
real *syn0, *syn1, *syn1neg, *h_expTable;

clock_t start;

int hs = 0, negative = 5;
const int table_size = 1e8;
int *table;//unigram table

vector<int*> d_sen_v;
vector<int*> sen_v;

void InitGPU(GPUVariables& gpu_var){
				
	checkCUDAError(cudaMalloc((void**)&gpu_var.d_syn0, sizeof(real) * vocab_size * layer1_size));

	if (hs){
		checkCUDAError(cudaMalloc((void**)&gpu_var.d_syn1, sizeof(real)*vocab_size*layer1_size));
		checkCUDAError(cudaMemset(gpu_var.d_syn1, 0, sizeof(real)*vocab_size*layer1_size));
	}
	if (negative > 0){
		checkCUDAError(cudaMalloc((void**)&gpu_var.d_syn1neg, sizeof(real)*vocab_size*layer1_size));
		checkCUDAError(cudaMemset(gpu_var.d_syn1neg, 0, sizeof(real)*vocab_size*layer1_size));

		checkCUDAError(cudaMalloc((void**)&gpu_var.d_table, table_size*sizeof(int)));
		checkCUDAError(cudaMemcpy(gpu_var.d_table, table, table_size*sizeof(int), cudaMemcpyHostToDevice));

		checkCUDAError(cudaMalloc((void**)&gpu_var.d_randoms, MAX_SENTENCE_LENGTH*sizeof(long)));
		vector<unsigned long> h_random(MAX_SENTENCE_LENGTH);

		for (int i = 0; i < MAX_SENTENCE_LENGTH; i++) h_random[i] = rand();

		checkCUDAError(cudaMemcpy(gpu_var.d_randoms, h_random.data(), MAX_SENTENCE_LENGTH*sizeof(long), cudaMemcpyHostToDevice));
		
	}

	checkCUDAError(cudaMemcpy(gpu_var.d_syn0, syn0, sizeof(real)*vocab_size*layer1_size, cudaMemcpyHostToDevice));

	checkCUDAError(cudaMemcpyToSymbol(expTable, h_expTable, sizeof(real) * (EXP_TABLE_SIZE + 1),0,cudaMemcpyHostToDevice));
	//checkCUDAError(cudaMalloc((void**)&gpu_var.d_expTable, (EXP_TABLE_SIZE + 1)*sizeof(real)));
	//checkCUDAError(cudaMemcpy(gpu_var.d_expTable, expTable, (EXP_TABLE_SIZE + 1)*sizeof(real), cudaMemcpyHostToDevice));

	checkCUDAError(cudaMalloc((void**)&gpu_var.d_vocab_codelen, (vocab_size + 1)*sizeof(int)));

	checkCUDAError(cudaMemcpy(gpu_var.d_vocab_codelen, vocab_codelen, (vocab_size + 1)*sizeof(int), cudaMemcpyHostToDevice));
	checkCUDAError(cudaMalloc((void**)&gpu_var.d_vocab_code, vocab_codelen[vocab_size] * sizeof(char)));
	checkCUDAError(cudaMalloc((void**)&gpu_var.d_vocab_point, vocab_codelen[vocab_size] * sizeof(int)));
	checkCUDAError(cudaMemcpy(gpu_var.d_vocab_code, vocab_code, vocab_codelen[vocab_size] * sizeof(char), cudaMemcpyHostToDevice));
	checkCUDAError(cudaMemcpy(gpu_var.d_vocab_point, vocab_point, vocab_codelen[vocab_size] * sizeof(int), cudaMemcpyHostToDevice));

	if (knnn){
		checkCUDAError(cudaMalloc((void**)&gpu_var.l2norms, sizeof(real) * vocab_size));
		checkCUDAError(cudaMalloc((void**)&gpu_var.distances, sizeof(Similarity) * vocab_size));
		checkCUDAError(cudaMalloc((void**)&gpu_var.topks, sizeof(int) * vocab_size * K));		
	}
}

void InitUnigramTable() {
	int a, i;
	long long train_words_pow = 0;
	real d1, power = 0.75;
	table = (int *)malloc(table_size * sizeof(int));
	
	for (a = 0; a < vocab_size; a++) train_words_pow += pow(vocab[a].cn, power);
	i = 0;
	d1 = pow(vocab[i].cn, power) / train_words_pow;
	for (a = 0; a < table_size; a++) {
		table[a] = i;
		if (a / (double)table_size > d1) {
			i++;
			d1 += pow(vocab[i].cn, power) /train_words_pow;
		}
		if (i >= vocab_size) i = vocab_size - 1;
	}	
}

// Reads a single word from a file, assuming space + tab + EOL to be word boundaries
void ReadWord(char *word, FILE *fin, char *eof) {
	int a = 0, ch;
	while (1) {
		ch = fgetc_unlocked(fin);
		if (ch == EOF) {
			*eof = 1;
			break;
		}
		if (ch == 13) continue;
		if ((ch == ' ') || (ch == '\t') || (ch == '\n')) {
			if (a > 0) {
				if (ch == '\n') ungetc(ch, fin);
				break;
			}
			if (ch == '\n') {
				strcpy(word, (char *)"</s>");
				return;
			}
			else continue;
		}
		word[a] = ch;
		a++;
		if (a >= MAX_STRING - 1) a--;   // Truncate too long words
	}
	word[a] = 0;
}

// Returns hash value of a word
int GetWordHash(char *word) {
	unsigned long long a, hash = 0;
	int len = strlen(word);
	for (a = 0; a < len; a++) hash = hash * 257 + word[a];
	hash = hash % vocab_hash_size;
	return hash;
}

// Returns position of a word in the vocabulary; if the word is not found, returns -1
int SearchVocab(char *word) {
	unsigned int hash = GetWordHash(word);
	while (1) {
		if (vocab_hash[hash] == -1) return -1;
		if (!strcmp(word, vocab[vocab_hash[hash]].word)) return vocab_hash[hash];
		hash = (hash + 1) % vocab_hash_size;
	}
	return -1;
}

// Reads a word and returns its index in the vocabulary
int ReadWordIndex(FILE *fin, char *eof) {
	char word[MAX_STRING], eof_l = 0;
	ReadWord(word, fin, &eof_l);
	if (eof_l) {
		*eof = 1;
		return -1;
	}
	return SearchVocab(word);
}

// Adds a word to the vocabulary
int AddWordToVocab(char *word) {
	unsigned int hash, length = strlen(word) + 1;
	if (length > MAX_STRING) length = MAX_STRING;
	vocab[vocab_size].word = (char *)calloc(length, sizeof(char));
	strcpy(vocab[vocab_size].word, word);
	vocab[vocab_size].cn = 0;
	vocab_size++;
	// Reallocate memory if needed
	if (vocab_size + 2 >= vocab_max_size) {
		vocab_max_size += 1000;
		vocab = (struct vocab_word *)realloc(vocab, vocab_max_size * sizeof(struct vocab_word));
	}
	hash = GetWordHash(word);
	while (vocab_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
	vocab_hash[hash] = vocab_size - 1;
	return vocab_size - 1;
}

// Used later for sorting by word counts
int VocabCompare(const void *a, const void *b) {
	long long l = ((struct vocab_word *)b)->cn - ((struct vocab_word *)a)->cn;
	if (l > 0) return 1;
	if (l < 0) return -1;
	return 0;
}

// Sorts the vocabulary by frequency using word counts
void SortVocab() {
	int a, size;
	unsigned int hash;
	// Sort the vocabulary and keep </s> at the first position
	qsort(&vocab[1], vocab_size - 1, sizeof(struct vocab_word), VocabCompare);
	for (a = 0; a < vocab_hash_size; a++) vocab_hash[a] = -1;
	size = vocab_size;
	train_words = 0;
	for (a = 0; a < size; a++) {
		// Words occuring less than min_count times will be discarded from the vocab
		if ((vocab[a].cn < min_count) && (a != 0)) {
			vocab_size--;
			free(vocab[a].word);
		}
		else {
			// Hash will be re-computed, as after the sorting it is not actual
			hash = GetWordHash(vocab[a].word);
			while (vocab_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
			vocab_hash[hash] = a;
			train_words += vocab[a].cn;
		}
	}
	vocab = (struct vocab_word *)realloc(vocab, (vocab_size + 1) * sizeof(struct vocab_word));
	// Allocate memory for the binary tree construction
	for (a = 0; a < vocab_size; a++) {
		vocab[a].code = (char *)calloc(MAX_CODE_LENGTH, sizeof(char));
		vocab[a].point = (int *)calloc(MAX_CODE_LENGTH, sizeof(int));
	}
}

// Reduces the vocabulary by removing infrequent tokens
void ReduceVocab() {
	int a, b = 0;
	unsigned int hash;
	for (a = 0; a < vocab_size; a++) if (vocab[a].cn > min_reduce) {
		vocab[b].cn = vocab[a].cn;
		vocab[b].word = vocab[a].word;
		b++;
	}
	else free(vocab[a].word);
	vocab_size = b;
	for (a = 0; a < vocab_hash_size; a++) vocab_hash[a] = -1;
	for (a = 0; a < vocab_size; a++) {
		// Hash will be re-computed, as it is not actual
		hash = GetWordHash(vocab[a].word);
		while (vocab_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
		vocab_hash[hash] = a;
	}
	fflush(stdout);
	min_reduce++;
}

// Create binary Huffman tree using the word counts
// Frequent words will have short uniqe binary codes
void CreateBinaryTree() {
	long long a, b, i, min1i, min2i, pos1, pos2, point[MAX_CODE_LENGTH];
	char code[MAX_CODE_LENGTH];
	long long *count = (long long *)calloc(vocab_size * 2 + 1, sizeof(long long));
	long long *binary = (long long *)calloc(vocab_size * 2 + 1, sizeof(long long));
	long long *parent_node = (long long *)calloc(vocab_size * 2 + 1, sizeof(long long));
	for (a = 0; a < vocab_size; a++) count[a] = vocab[a].cn;
	for (a = vocab_size; a < vocab_size * 2; a++) count[a] = 1e15;
	pos1 = vocab_size - 1;
	pos2 = vocab_size;
	// Following algorithm constructs the Huffman tree by adding one node at a time
	for (a = 0; a < vocab_size - 1; a++) {
		// First, find two smallest nodes 'min1, min2'
		if (pos1 >= 0) {
			if (count[pos1] < count[pos2]) {
				min1i = pos1;
				pos1--;
			}
			else {
				min1i = pos2;
				pos2++;
			}
		}
		else {
			min1i = pos2;
			pos2++;
		}
		if (pos1 >= 0) {
			if (count[pos1] < count[pos2]) {
				min2i = pos1;
				pos1--;
			}
			else {
				min2i = pos2;
				pos2++;
			}
		}
		else {
			min2i = pos2;
			pos2++;
		}
		count[vocab_size + a] = count[min1i] + count[min2i];
		parent_node[min1i] = vocab_size + a;
		parent_node[min2i] = vocab_size + a;
		binary[min2i] = 1;
	}
	// Now assign binary code to each vocabulary word
	for (a = 0; a < vocab_size; a++) {
		b = a;
		i = 0;
		while (1) {
			code[i] = binary[b];
			point[i] = b;
			i++;
			b = parent_node[b];
			if (b == vocab_size * 2 - 2) break;
		}
		vocab[a].codelen = i;
		vocab[a].point[0] = vocab_size - 2;
		for (b = 0; b < i; b++) {
			vocab[a].code[i - b - 1] = code[b];
			vocab[a].point[i - b] = point[b] - vocab_size;
		}
	}
	free(count);
	free(binary);
	free(parent_node);
}

void LearnVocabFromTrainFile() {
	char word[MAX_STRING], eof = 0;
	FILE *fin;
	long long a, i, wc = 0;
	for (a = 0; a < vocab_hash_size; a++) vocab_hash[a] = -1;
	fin = fopen(train_file, "rb");
	if (fin == NULL) {
		printf("ERROR: training data file not found!\n");
		exit(1);
	}
	vocab_size = 0;
	AddWordToVocab((char *)"</s>");
	while (1) {
		ReadWord(word, fin, &eof);
		if (eof) break;
		train_words++;
		wc++;
		if ((debug_mode > 1) && (wc >= 1000000)) {
			printf("%lldM%c", train_words / 1000000, 13);
			fflush(stdout);
			wc = 0;
		}
		i = SearchVocab(word);
		if (i == -1) {
			a = AddWordToVocab(word);
			vocab[a].cn = 1;
		}
		else vocab[i].cn++;
		if (vocab_size > vocab_hash_size * 0.7) ReduceVocab();
	}
	SortVocab();
	if (debug_mode > 0) {
		printf("Vocab size: %lld\n", vocab_size);
		printf("Words in train file: %lld\n", train_words);
	}
	file_size = ftell(fin);
	fclose(fin);
}

void SaveVocab() {
	long long i;
	FILE *fo = fopen(save_vocab_file, "wb");
	for (i = 0; i < vocab_size; i++) fprintf(fo, "%s %lld\n", vocab[i].word, vocab[i].cn);
	fclose(fo);
}

void ReadVocab() {
	long long a, i = 0;
	char c, eof = 0;
	char word[MAX_STRING];
	FILE *fin = fopen(read_vocab_file, "rb");
	if (fin == NULL) {
		printf("Vocabulary file not found\n");
		exit(1);
	}
	for (a = 0; a < vocab_hash_size; a++) vocab_hash[a] = -1;
	vocab_size = 0;
	while (1) {
		ReadWord(word, fin, &eof);
		if (eof) break;
		a = AddWordToVocab(word);
		fscanf(fin, "%lld%c", &vocab[a].cn, &c);
		i++;
	}
	SortVocab();
	if (debug_mode > 0) {
		printf("Vocab size: %lld\n", vocab_size);
		printf("Words in train file: %lld\n", train_words);
	}
	fin = fopen(train_file, "rb");
	if (fin == NULL) {
		printf("ERROR: training data file not found!\n");
		exit(1);
	}
	fseek(fin, 0, SEEK_END);
	file_size = ftell(fin);
	fclose(fin);
}

void InitNet() {
	long long a, b;
	rand_type next_random = 1;
	a = posix_memalign((void **)&syn0, 128, (long long)vocab_size * layer1_size * sizeof(real));
	if (syn0 == NULL) { printf("Memory allocation failed\n"); exit(1); }

	if (hs) {
		a = posix_memalign((void **)&syn1, 128, (long long)vocab_size * layer1_size * sizeof(real));
		if (syn1 == NULL) { printf("Memory allocation failed\n"); exit(1); }		
	}
	if (negative > 0) {
		a = posix_memalign((void **)&syn1neg, 128, (long long)vocab_size * layer1_size * sizeof(real));
		if (syn1neg == NULL) { printf("Memory allocation failed\n"); exit(1); }		
	}
	double start = gettime();
	for (a = 0; a < vocab_size; a++) {
		for (b = 0; b < layer1_size; b++) {
			next_random = next_random * RAND_MAGIC;
			syn0[a * layer1_size + b] = (((next_random & 0xFFFF) / (real)65536) - 0.5) / layer1_size;
		}
	}
	fprintf(stderr, "%.3lfs to init with random weights\n", gettime() - start);
	
	start = gettime();

	CreateBinaryTree();

	fprintf(stderr, "%.3lfs to create binary tree\n", gettime() - start);
}

void *TrainModelThread(long long id, int *cnt, GPUVariables& gpu_var, int use_unigram_table_kernel_first) {
	long long word, sentence_length = 0, sentence_position = 0;
	long long word_count = 0, last_word_count = 0;
		
	long long local_iter = iter;
	rand_type next_random = (long)id;
	char eof = 0;
	clock_t now;
	
	FILE *fi = fopen(train_file, "rb");
	fseek(fi, (file_size / (long long)num_threads) * id, SEEK_SET);

	int *sen = sen_v[id];
	int *d_sen = d_sen_v[id];
	real *d_syn0 = gpu_var.d_syn0;
	real *d_syn1 = gpu_var.d_syn1;
	real *d_syn1neg = gpu_var.d_syn1neg;
	real *d_expTable = gpu_var.d_expTable;
	int *d_table = gpu_var.d_table;
	int *d_topks = gpu_var.topks;
	rand_type *d_random = gpu_var.d_randoms;

	int *d_vocab_codelen = gpu_var.d_vocab_codelen;
	char *d_vocab_code = gpu_var.d_vocab_code;
	int *d_vocab_point = gpu_var.d_vocab_point;

	
	while (1) {
		if (word_count - last_word_count > 10000) {
			#pragma omp atomic
			word_count_actual += word_count - last_word_count;
			last_word_count = word_count;
#pragma omp single nowait
			if ((debug_mode > 1)) {
				now = clock();
				printf("%cAlpha: %f  Progress: %.2f%%  Words/thread/sec: %.2fk  ", 13, alpha,
					word_count_actual / (real)(iter * train_words + 1) * 100,
					word_count_actual / ((real)(now - start + 1) / (real)CLOCKS_PER_SEC * 1000));
				fflush(stdout);
			}
			alpha = starting_alpha * (1 - word_count_actual / (real)(iter * train_words + 1));
			if (alpha < starting_alpha * 0.0001) alpha = starting_alpha * 0.0001;
		}

		sentence_length = 0;
		while (1) {
			word = ReadWordIndex(fi, &eof);
			if (feof(fi)) break;
			if (word == -1) continue;
			word_count++;
			if (word == 0) break;
			// The subsampling randomly discards frequent words while keeping the ranking same
			if (sample > 0) {
				real ran = (sqrt(vocab[word].cn / (sample * train_words)) + 1) * (sample * train_words) / vocab[word].cn;			
				next_random = next_random * RAND_MAGIC;
				if (ran < (next_random & 0xFFFF) / (real)65536) continue;
			}
			sen[sentence_length] = word;
			sentence_length++;
			if (sentence_length >= MAX_SENTENCE_LENGTH) break;
		}

		if (eof || (word_count > train_words / num_threads)) {
		
		#pragma omp atomic
			word_count_actual += word_count - last_word_count;		
			break;
		}

		if (cbow) {  //train the cbow architecture
			// in -> hidden
			checkCUDAError(cudaMemcpyAsync(d_sen, sen, sentence_length * sizeof(int), cudaMemcpyHostToDevice));

			if (knnn == 0){
				cbow_cuda(window, negative, alpha, sentence_length, d_sen, layer1_size, d_syn0, hs, d_syn1,
					d_expTable, d_vocab_codelen, d_vocab_code, d_vocab_point, d_table, table_size, vocab_size, d_syn1neg, d_random);
				//checkCUDAError(cudaDeviceSynchronize());
			}
			else{

				//The first iteration uses the unigram table normally for the negative sampling. This is a test, since the first iteration has a random matrix, and that might affect the knn
				if (use_unigram_table_kernel_first){
					cbow_cuda(window, negative, alpha, sentence_length, d_sen, layer1_size, d_syn0, hs, d_syn1,
						d_expTable, d_vocab_codelen, d_vocab_code, d_vocab_point, d_table, table_size, vocab_size, d_syn1neg, d_random);
					//checkCUDAError(cudaDeviceSynchronize());
				}
				else{
					cbow_cuda_knnn(window, negative, alpha, sentence_length, d_sen, layer1_size, d_syn0, hs, d_syn1,
						d_expTable, d_vocab_codelen, d_vocab_code, d_vocab_point, d_table, table_size, vocab_size, d_syn1neg, d_topks, d_random, K, METHOD);
					//checkCUDAError(cudaDeviceSynchronize());
				}

			}
			
			
		}
		else {  //train skip-gram
				
		}
		(*cnt)++;
	}
		
	fclose(fi);
	//pthread_exit(NULL);
}

void InitVocabCode(){
	//rogy
	printf("vocab size = %d\n", vocab_size);
	vocab_codelen = (int*)malloc((vocab_size + 1)*sizeof(int));

	assert(NULL != vocab_codelen);
	vocab_codelen[0] = 0;
	for (int i = 1; i < vocab_size + 1; i++){
		vocab_codelen[i] = vocab_codelen[i - 1] + vocab[i - 1].codelen;
		//	printf("codelen of %d is %d\n", i, vocab_codelen[i]);
	}

	vocab_code = (char*)malloc(vocab_codelen[vocab_size] * sizeof(char));
	assert(NULL != vocab_code);

	vocab_point = (int*)malloc(vocab_codelen[vocab_size] * sizeof(int));
	assert(NULL != vocab_code);

	for (int i = 0; i < vocab_size; i++){
		for (int j = 0; j < vocab[i].codelen; j++){
			vocab_code[vocab_codelen[i] + j] = vocab[i].code[j];
			vocab_point[vocab_codelen[i] + j] = vocab[i].point[j];
		}
	}

}

void syncMatrixes(vector<GPUVariables> &gpu_vars, real *syn0, real *syn0t, real *syn1,
	real *syn1t, real *syn1negt, real *syn1negt2){
	
	//For syn0
	checkCUDAError(cudaMemcpy(syn0, gpu_vars[0].d_syn0, vocab_size*layer1_size*sizeof(real), cudaMemcpyDeviceToHost));

	for (int id = 1; id < num_gpus; id++){
		checkCUDAError(cudaSetDevice(id));
		checkCUDAError(cudaMemcpy(syn0t, gpu_vars[id].d_syn0, vocab_size*layer1_size*sizeof(real), cudaMemcpyDeviceToHost));

		//Sum all syn0
#pragma omp parallel for num_threads(4)
		for (long long a = 0; a < vocab_size; a++)
			for (long long b = 0; b < layer1_size; b++){
				long long pos = a * layer1_size + b;
				syn0[pos] += syn0t[pos];
			}
	}
	//Average of syn0
#pragma omp parallel for num_threads(4)
	for (long long a = 0; a < vocab_size; a++)
		for (long long b = 0; b < layer1_size; b++){
			syn0[a * layer1_size + b] /= num_gpus;
		}

	//For syn1
	if (hs){
		checkCUDAError(cudaMemcpy(syn1, gpu_vars[0].d_syn1, vocab_size*layer1_size*sizeof(real), cudaMemcpyDeviceToHost));

		for (int id = 1; id < num_threads; id++){
			checkCUDAError(cudaSetDevice(id));
			checkCUDAError(cudaMemcpy(syn1t, gpu_vars[id].d_syn1, vocab_size*layer1_size*sizeof(real), cudaMemcpyDeviceToHost));

			//Sum all syn1
#pragma omp parallel for num_threads(4)
			for (long long a = 0; a < vocab_size; a++)
				for (long long b = 0; b < layer1_size; b++){
					long long pos = a * layer1_size + b;
					syn1[pos] += syn1t[pos];
				}
		}
		//Average of syn1
#pragma omp parallel for num_threads(4)
		for (long long a = 0; a < vocab_size; a++)
			for (long long b = 0; b < layer1_size; b++){
				syn1[a * layer1_size + b] /= num_gpus;
			}
	}

	//For syn1neg
	if (negative > 0 && syncSyn1Neg > 0){
		checkCUDAError(cudaMemcpy(syn1negt, gpu_vars[0].d_syn1neg, vocab_size*layer1_size*sizeof(real), cudaMemcpyDeviceToHost));

		for (int id = 1; id < num_gpus; id++){
			checkCUDAError(cudaSetDevice(id));
			checkCUDAError(cudaMemcpy(syn1negt2, gpu_vars[id].d_syn1neg, vocab_size*layer1_size*sizeof(real), cudaMemcpyDeviceToHost));

			//Sum all syn1neg
#pragma omp parallel for num_threads(4)
			for (long long a = 0; a < vocab_size; a++)
				for (long long b = 0; b < layer1_size; b++){
					long long pos = a * layer1_size + b;
					syn1negt[pos] += syn1negt2[pos];
				}
		}
		//Average of syn1neg
#pragma omp parallel for num_threads(4)
		for (long long a = 0; a < vocab_size; a++)
			for (long long b = 0; b < layer1_size; b++){
				syn1negt[a * layer1_size + b] /= num_gpus;
			}

	}
	//broadcast to GPUs
	for (int id = 0; id < num_gpus; id++){
		checkCUDAError(cudaSetDevice(id));
		checkCUDAError(cudaMemcpyAsync(gpu_vars[id].d_syn0, syn0, vocab_size * layer1_size * sizeof(real), cudaMemcpyHostToDevice));

		if (hs)
			checkCUDAError(cudaMemcpyAsync(gpu_vars[id].d_syn1, syn1, vocab_size * layer1_size * sizeof(real), cudaMemcpyHostToDevice));

		if (negative > 0 && syncSyn1Neg > 0)
			checkCUDAError(cudaMemcpyAsync(gpu_vars[id].d_syn1neg, syn1negt, vocab_size * layer1_size * sizeof(real), cudaMemcpyHostToDevice));

	}


}

void TrainModel() {
	long long a=1, b, c, d;
	double start;
	FILE *fo;

	//srand(time(NULL));

	//pthread_t *pt = (pthread_t *)malloc(num_threads * sizeof(pthread_t));
	printf("Starting training using file %s\n", train_file);
	starting_alpha = alpha;
	if (read_vocab_file[0] != 0){
		start = gettime();
		ReadVocab();
		fprintf(stderr, "%.2lfs to read vocab from train file\n", gettime() - start);
	}
	else{ 
		start = gettime();
		LearnVocabFromTrainFile(); 
		fprintf(stderr, "%.2lfs to learn vocab from train file\n", gettime() - start);
	}
	if (save_vocab_file[0] != 0) SaveVocab();
	if (output_file[0] == 0) return;
	printf("Initialising matrixes\n");
	InitNet();
	if (negative > 0){
		printf("Creating Unigram Table\n");
		start = gettime();
		InitUnigramTable();
		fprintf(stderr, "%.2lfs to create unigram table\n", gettime() - start);
	}
	
	InitVocabCode();

	double sync_time = 0, knn_time = 0;

	start = gettime();
	
	vector<GPUVariables> gpu_vars(num_gpus);
	
	//Setting each GPU variables
	#pragma omp parallel num_threads(num_gpus)
	{		
		int id = omp_get_thread_num();
				
		checkCUDAError(cudaSetDevice(id));

		GPUVariables gpu_var;

		InitGPU(gpu_var);

		gpu_vars[id] = gpu_var;
	}

	d_sen_v.resize(num_threads);
	sen_v.resize(num_threads);

	//Setting each thread with its corresponding GPU and host allocation
	#pragma omp parallel num_threads(num_threads)
	{
		int id = omp_get_thread_num();

		checkCUDAError(cudaSetDevice(id % num_gpus));
		checkCUDAError(cudaMalloc((void**)&d_sen_v[id], (MAX_SENTENCE_LENGTH + 1)*sizeof(int)));
		checkCUDAError(cudaMallocHost((void**)&sen_v[id], (MAX_SENTENCE_LENGTH + 1)*sizeof(int)));
	}


	fprintf(stderr, "%.2lfs to init GPU\n", gettime() - start);

	//for (a = 0; a < num_threads; a++) pthread_create(&pt[a], NULL, TrainModelThread, (void *)a);
	real *syn0t;
	real *syn1t;
	real *syn1negt,*syn1negt2;
	int *topks;
	if (num_gpus > 1){
		syn0t = (real*)malloc(vocab_size * layer1_size * sizeof(real));	
		
		if (hs)
			syn1t = (real*)malloc(vocab_size * layer1_size * sizeof(real));

		if (negative > 0){
			syn1negt = (real*)malloc(vocab_size * layer1_size * sizeof(real));
			syn1negt2 = (real*)malloc(vocab_size * layer1_size * sizeof(real));
		}

		if (knnn){
			topks = (int*)malloc(sizeof(int) * vocab_size * K);
		}
	}
		
	start = gettime();

		
	for (int local_iter = 0; local_iter < iter; local_iter++){
				
		#pragma omp parallel num_threads(num_threads)
		{
			int cnt = 0;
			int id = omp_get_thread_num();

			checkCUDAError(cudaSetDevice(id % num_gpus));

			//int use_unigram_table_first = (local_iter <= K_iter_start);
						
			if (local_iter <= K_iter_start)
				TrainModelThread(id, &cnt, gpu_vars[id % num_gpus], 1);
			else
				TrainModelThread(id, &cnt, gpu_vars[id % num_gpus], 0);
			
			if (local_iter == iter - 1)
				fprintf(stderr, "count %d\n", cnt * iter);
		}

		//Sync at every ITERS_TO_SYNC or at the last iteration
		if (local_iter % ITERS_TO_SYNC == 0	|| local_iter == iter - 1){

			//For 1 GPU only
			if (num_gpus == 1 && local_iter == iter - 1){
				checkCUDAError(cudaMemcpy(syn0, gpu_vars[0].d_syn0, vocab_size*layer1_size*sizeof(real), cudaMemcpyDeviceToHost));
			}
			if (num_gpus > 1){
				double st = gettime();

				syncMatrixes(gpu_vars, syn0, syn0t, syn1, syn1t, syn1negt, syn1negt2);

				sync_time += gettime() - st;

			}
		}
		if (knnn && local_iter < iter - 1 && local_iter >= K_iter_start){//knn
			double st = gettime();

			#pragma omp parallel num_threads(num_gpus)
			{
				int id = omp_get_thread_num();
				checkCUDAError(cudaSetDevice(id));

				int loop_cnt = ((int)ceil(vocab_size / (double)num_gpus));
				int l_input_size = (id + 1) * loop_cnt > vocab_size ? vocab_size - id * loop_cnt : loop_cnt;
				int offset = id * loop_cnt;

				//calculate the l2 norms
				l2norm_calc_kernel << <128, blockSize >> >(layer1_size, gpu_vars[id].d_syn0, vocab_size, gpu_vars[id].l2norms);
				checkCUDAError(cudaDeviceSynchronize());

				thrust::device_ptr<Similarity> thrust_dist(gpu_vars[id].distances);

				cached_allocator cache;
				
				//calculate the cosine distance and the K farest neighbors for each word, one at a time
				for (int word = 0 + (id == 0 ? 1 : 0); word < l_input_size; word++)
				{
					if (word % 10000 == 0 && id == 0)cerr << word << endl;
					
					cosine_distance_calc_kernel << <256, 256 >> >(word + offset, layer1_size, gpu_vars[id].d_syn0,
						vocab_size, gpu_vars[id].l2norms, gpu_vars[id].distances);

					//checkCUDAError(cudaDeviceSynchronize());

					//Sort normally since the worst neighbors will have similarity close to 0
					if (METHOD >= 3)
						thrust::sort(thrust::cuda::par(cache), thrust_dist + 1, thrust_dist + vocab_size,thrust::greater<Similarity>());
					else
						thrust::sort(thrust::cuda::par(cache), thrust_dist + 1, thrust_dist + vocab_size);
					//checkCUDAError(cudaDeviceSynchronize());

					//Select the first K only
					write_topk_kernel << <1, K >> >(word + offset, K, gpu_vars[id].distances, gpu_vars[id].topks);
					//checkCUDAError(cudaDeviceSynchronize());
					
				}

				//share topKs between GPUs
				if (num_gpus > 1){
					//copy each part
					checkCUDAError(cudaMemcpy(topks + offset, gpu_vars[id].topks + offset, sizeof(int) * K * l_input_size, cudaMemcpyDeviceToHost));

				}								

				fprintf(stderr, "gpu %d finished knn\n", id);
			}

			if (num_gpus > 1)
			for (int i = 0; i < num_gpus; i++){

				checkCUDAError(cudaSetDevice(i));				
				checkCUDAError(cudaMemcpy(gpu_vars[i].topks, topks, sizeof(int) * K * vocab_size, cudaMemcpyHostToDevice));
			}

			knn_time += gettime() - st;
		}
	}


	//for (a = 0; a < num_threads; a++) pthread_join(pt[a], NULL);

	fprintf(stderr, "%.2lfs to train\n", gettime() - start);
	if (num_gpus > 1)
		fprintf(stderr, "%.2lfs spent on models sync\n", sync_time);
	if (knnn > 0)
		fprintf(stderr, "%.2lfs spent on knn\n", knn_time);
	//rogy
	free(vocab_codelen);
	free(vocab_code);
	free(vocab_point);
	if (num_gpus > 1){
		free(syn0t);

		if (hs)
			free(syn1t);

		if (negative > 0){
			free(syn1negt);
			free(syn1negt2);
		}
	}

	for (int i = 0; i < sen_v.size(); i++)
		checkCUDAError(cudaFreeHost(sen_v[i]));
		
	fo = fopen(output_file, "wb");
	if (classes == 0) {
		// Save the word vectors
		fprintf(fo, "%lld %lld\n", vocab_size, layer1_size);
		for (a = 0; a < vocab_size; a++) {
			fprintf(fo, "%s ", vocab[a].word);
			if (binary) for (b = 0; b < layer1_size; b++) fwrite(&syn0[a * layer1_size + b], sizeof(real), 1, fo);
			else for (b = 0; b < layer1_size; b++) fprintf(fo, "%lf ", syn0[a * layer1_size + b]);
			fprintf(fo, "\n");
		}
	}
	else {
		// Run K-means on the word vectors
		int clcn = classes, iter = 10, closeid;
		int *centcn = (int *)malloc(classes * sizeof(int));
		int *cl = (int *)calloc(vocab_size, sizeof(int));
		real closev, x;
		real *cent = (real *)calloc(classes * layer1_size, sizeof(real));
		for (a = 0; a < vocab_size; a++) cl[a] = a % clcn;
		for (a = 0; a < iter; a++) {
			for (b = 0; b < clcn * layer1_size; b++) cent[b] = 0;
			for (b = 0; b < clcn; b++) centcn[b] = 1;
			for (c = 0; c < vocab_size; c++) {
				for (d = 0; d < layer1_size; d++) cent[layer1_size * cl[c] + d] += syn0[c * layer1_size + d];
				centcn[cl[c]]++;
			}
			for (b = 0; b < clcn; b++) {
				closev = 0;
				for (c = 0; c < layer1_size; c++) {
					cent[layer1_size * b + c] /= centcn[b];
					closev += cent[layer1_size * b + c] * cent[layer1_size * b + c];
				}
				closev = sqrt(closev);
				for (c = 0; c < layer1_size; c++) cent[layer1_size * b + c] /= closev;
			}
			for (c = 0; c < vocab_size; c++) {
				closev = -10;
				closeid = 0;
				for (d = 0; d < clcn; d++) {
					x = 0;
					for (b = 0; b < layer1_size; b++) x += cent[layer1_size * d + b] * syn0[c * layer1_size + b];
					if (x > closev) {
						closev = x;
						closeid = d;
					}
				}
				cl[c] = closeid;
			}
		}
		// Save the K-means classes
		for (a = 0; a < vocab_size; a++) fprintf(fo, "%s %d\n", vocab[a].word, cl[a]);
		free(centcn);
		free(cent);
		free(cl);
	}
	fclose(fo);
}

int ArgPos(char *str, int argc, char **argv) {
	int a;
	for (a = 1; a < argc; a++) if (!strcmp(str, argv[a])) {
		if (a == argc - 1) {
			printf("Argument missing for %s\n", str);
			exit(1);
		}
		return a;
	}
	return -1;
}

int main(int argc, char **argv) {
	int i;
	if (argc == 1) {
		printf("WORD VECTOR estimation toolkit v 0.1c\n\n");
		printf("Options:\n");
		printf("Parameters for training:\n");
		printf("\t-train <file>\n");
		printf("\t\tUse text data from <file> to train the model\n");
		printf("\t-output <file>\n");
		printf("\t\tUse <file> to save the resulting word vectors / word clusters\n");
		printf("\t-size <int>\n");
		printf("\t\tSet size of word vectors; default is 100\n");
		printf("\t-window <int>\n");
		printf("\t\tSet max skip length between words; default is 5\n");
		printf("\t-sample <float>\n");
		printf("\t\tSet threshold for occurrence of words. Those that appear with higher frequency in the training data\n");
		printf("\t\twill be randomly down-sampled; default is 1e-3, useful range is (0, 1e-5)\n");
		printf("\t-hs <int>\n");
		printf("\t\tUse Hierarchical Softmax; default is 0 (not used)\n");
		printf("\t-negative <int>\n");
		printf("\t\tNumber of negative examples; default is 5, common values are 3 - 10 (0 = not used)\n");
		printf("\t-threads <int>\n");
		printf("\t\tUse <int> threads (default 12)\n");
		printf("\t-iter <int>\n");
		printf("\t\tRun more training iterations (default 5)\n");
		printf("\t-min-count <int>\n");
		printf("\t\tThis will discard words that appear less than <int> times; default is 5\n");
		printf("\t-alpha <float>\n");
		printf("\t\tSet the starting learning rate; default is 0.025 for skip-gram and 0.05 for CBOW\n");
		printf("\t-classes <int>\n");
		printf("\t\tOutput word classes rather than word vectors; default number of classes is 0 (vectors are written)\n");
		printf("\t-debug <int>\n");
		printf("\t\tSet the debug mode (default = 2 = more info during training)\n");
		printf("\t-binary <int>\n");
		printf("\t\tSave the resulting vectors in binary moded; default is 0 (off)\n");
		printf("\t-save-vocab <file>\n");
		printf("\t\tThe vocabulary will be saved to <file>\n");
		printf("\t-read-vocab <file>\n");
		printf("\t\tThe vocabulary will be read from <file>, not constructed from the training data\n");
		printf("\t-cbow <int>\n");
		printf("\t\tUse the continuous bag of words model; default is 1 (use 0 for skip-gram model)\n");
		printf("\nExamples:\n");
		printf("./word2vec -train data.txt -output vec.txt -size 200 -window 5 -sample 1e-4 -negative 5 -hs 0 -binary 0 -cbow 1 -iter 3\n\n");
		return 0;
	}

	output_file[0] = 0;
	save_vocab_file[0] = 0;
	read_vocab_file[0] = 0;
	if ((i = ArgPos((char *)"-size", argc, argv)) > 0) layer1_size = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-train", argc, argv)) > 0) strcpy(train_file, argv[i + 1]);
	if ((i = ArgPos((char *)"-save-vocab", argc, argv)) > 0) strcpy(save_vocab_file, argv[i + 1]);
	if ((i = ArgPos((char *)"-read-vocab", argc, argv)) > 0) strcpy(read_vocab_file, argv[i + 1]);
	if ((i = ArgPos((char *)"-debug", argc, argv)) > 0) debug_mode = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-binary", argc, argv)) > 0) binary = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-cbow", argc, argv)) > 0) cbow = atoi(argv[i + 1]);
	if (cbow) alpha = 0.05;
	if ((i = ArgPos((char *)"-alpha", argc, argv)) > 0) alpha = atof(argv[i + 1]);
	if ((i = ArgPos((char *)"-output", argc, argv)) > 0) strcpy(output_file, argv[i + 1]);
	if ((i = ArgPos((char *)"-window", argc, argv)) > 0) window = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-sample", argc, argv)) > 0) sample = atof(argv[i + 1]);
	if ((i = ArgPos((char *)"-hs", argc, argv)) > 0) hs = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-negative", argc, argv)) > 0) negative = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-threads", argc, argv)) > 0) threads_per_gpu = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-iter", argc, argv)) > 0) iter = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-min-count", argc, argv)) > 0) min_count = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-classes", argc, argv)) > 0) classes = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-use-warp", argc, argv)) > 0) useWarpApproach = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-blocksize", argc, argv)) > 0) blockSize = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-sync-syn1neg", argc, argv)) > 0) syncSyn1Neg = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-num-gpus", argc, argv)) > 0) num_gpus = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-knnn", argc, argv)) > 0) knnn = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-K-iter-start", argc, argv)) > 0) K_iter_start = atoi(argv[i + 1]);
	if ((i = ArgPos((char *)"-method", argc, argv)) > 0) METHOD = atoi(argv[i + 1]);
	vocab = (struct vocab_word *)calloc(vocab_max_size, sizeof(struct vocab_word));
	vocab_hash = (int *)calloc(vocab_hash_size, sizeof(int));
	h_expTable = (real *)malloc((EXP_TABLE_SIZE + 1) * sizeof(real));
	for (i = 0; i < EXP_TABLE_SIZE + 1; i++) {
		h_expTable[i] = exp((i / (real)EXP_TABLE_SIZE * 2 - 1) * MAX_EXP); // Precompute the exp() table
		h_expTable[i] = h_expTable[i] / (h_expTable[i] + 1);                   // Precompute f(x) = x / (x + 1)
	}
	K = knnn;
	num_threads = threads_per_gpu * num_gpus;
	TrainModel();
	return 0;
}
