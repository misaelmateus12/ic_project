#include <cuda.h>
#include <cuda_runtime.h>
#include <algorithm>

__constant__ real expTable[EXP_TABLE_SIZE+1];

__device__ __forceinline__ real reduceInWarp(real f, int idInWarp){
	for (int i = warpSize / 2; i > 0; i >>= 1){
		f += __shfl_xor(f, i, 32);
	}
	return f;
}

template<long long hs>
void __global__ cbow_kernel(long window, long negative, float alpha, long sentence_length,
	const int* __restrict__ sen, long layer1_size, volatile float *syn0, volatile float *syn1,
	/*const float* __restrict__ expTable,*/ const int* __restrict__ vocab_codelen,
	const char* __restrict__ vocab_code, const int* __restrict__ vocab_point,
	const int* __restrict__ table, long table_size, long vocab_size, volatile float *syn1neg){

	extern __shared__ real s[]; //2*(real *)calloc(layer1_size, sizeof(real));

	int numWarpsPerBlock = blockDim.x / warpSize;
	int warpIdInBlock = threadIdx.x / warpSize;
	int warpId = warpIdInBlock + numWarpsPerBlock*blockIdx.x;
	int idInWarp = threadIdx.x % warpSize;

	real *neu1 = s + warpIdInBlock * layer1_size;
	real *neu1e = s + (numWarpsPerBlock + warpIdInBlock) * layer1_size;
	volatile int* cw = (volatile int*)(s + 2 * numWarpsPerBlock*layer1_size);//this is a vector per warp

	volatile unsigned long *temp_rand = (volatile unsigned long*)(s + 2 * numWarpsPerBlock*layer1_size) + numWarpsPerBlock;//this is a vector per warp

	for (int sentence_position = warpId; sentence_position < sentence_length; sentence_position += gridDim.x*numWarpsPerBlock){
		long long word = sen[sentence_position];
		if (word == -1) continue;
		if (0 == idInWarp) {
			temp_rand[warpIdInBlock] = sentence_position;
			temp_rand[warpIdInBlock] = temp_rand[warpIdInBlock] * (unsigned long)25214903917 + 11;
		}
		unsigned long next_random = temp_rand[warpIdInBlock];//rand();
		int b = next_random % window;

		for (int c = idInWarp; c < layer1_size; c += warpSize){
			neu1[c] = 0;
			neu1e[c] = 0;
		}

		// in -> hidden
		cw[warpIdInBlock] = 0;
		__syncthreads();

		for (int a = b; a < window * 2 + 1 - b; a++) 
			if (a != window) {
				int c = sentence_position - window + a;
				if (c < 0) continue;
				if (c >= sentence_length) continue;
				long long last_word = sen[c];
				if (last_word == -1) continue;

				for (int c = idInWarp; c < layer1_size; c += warpSize) 
					neu1[c] += syn0[c + last_word * layer1_size];

				if (idInWarp == 0) 
					cw[warpIdInBlock]++;
			}
		if (cw[warpIdInBlock]) {
			for (int c = idInWarp; c < layer1_size; c += warpSize) 
				neu1[c] /= cw[warpIdInBlock];

			if (hs){
				for (int d = vocab_codelen[word]; d < vocab_codelen[word + 1]; d++) {
					float f = 0;
					int l2 = vocab_point[d] * layer1_size;

					// Propagate hidden -> output
					for (int c = idInWarp; c < layer1_size; c += warpSize)
						f += neu1[c] * syn1[c + l2];

					f = reduceInWarp(f, idInWarp);
					if (f <= -MAX_EXP)
						continue;
					else
						if (f >= MAX_EXP)
							continue;
						else
							f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];

					// 'g' is the gradient multiplied by the learning rate
					float g = (1 - vocab_code[d] - f) * alpha;

					// Propagate errors output -> hidden
					for (int c = idInWarp; c < layer1_size; c += warpSize)
						neu1e[c] += g * syn1[c + l2];

					// Learn weights hidden -> output
					for (int c = idInWarp; c < layer1_size; c += warpSize)
						syn1[c + l2] += g * neu1[c];
				}
			}
			// NEGATIVE SAMPLING
			if (negative > 0) 
				for (int d = 0; d < negative + 1; d++) {
					int target;
					int label;
					if (d == 0) {
						target = word;
						label = 1;
					}
					else {
						if (0 == idInWarp) {
							temp_rand[warpIdInBlock] = temp_rand[warpIdInBlock] * (unsigned long)25214903917 + 11;
						}
						next_random = temp_rand[warpIdInBlock];
						//			
						target = table[(next_random >> 16) % table_size];
						if (target == 0) 
							target = next_random % (vocab_size - 1) + 1;

						if (target == word) 
							continue;
						label = 0;
					}
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = idInWarp; c < layer1_size; c += warpSize)
						f += neu1[c] * syn1neg[c + l2];

					f = reduceInWarp(f, idInWarp);
					if (f > MAX_EXP) 
						g = (label - 1) * alpha;
					else 
						if (f < -MAX_EXP) 
							g = (label - 0) * alpha;					
						else 
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = idInWarp; c < layer1_size; c += warpSize) 
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = idInWarp; c < layer1_size; c += warpSize) 
						syn1neg[c + l2] += g * neu1[c];
				}
			// hidden -> in
			for (int a = b; a < window * 2 + 1 - b; a++) 
				if (a != window) {
					int c = sentence_position - window + a;
					if (c < 0) continue;
					if (c >= sentence_length) continue;
					long long last_word = sen[c];
					if (last_word == -1) continue;
					for (int c = idInWarp; c < layer1_size; c += warpSize) 
						syn0[c + last_word * layer1_size] += neu1e[c];
				}
		}
	}
}


__device__ __forceinline__ real reduceInBlock(real f, int num_warps, real* sharedTemp){
	
	real temp = reduceInWarp(f, 0);

	if (threadIdx.x % warpSize == 0){
		sharedTemp[threadIdx.x / warpSize] = temp;
	}
	__syncthreads();
	
	/**
	if (threadIdx.x / warpSize == 0
		&& threadIdx.x < num_warps){ //Reduce the shared vector in the first warp
		temp = sharedTemp[threadIdx.x];

		for (int i = num_warps; i > 0; i >>= 1){
			temp += __shfl_xor(temp, i, 32);
		}
		if(threadIdx.x == 0)
			sharedTemp[0] = temp;
	}
	__syncthreads();
	return sharedTemp[0];/**/ // acho c�digo demais para fazer outra redu��o paralela

	//*
	real sum = sharedTemp[0];

	for (int i = 1; i < num_warps; i++)
		sum += sharedTemp[i];

	return sum;/**/
}

template<long long hs>
void __global__ cbow_kernel_block(long window, long negative, float alpha, long sentence_length,
	const int* __restrict__ sen, long layer1_size, volatile float *syn0, volatile float *syn1,
	/*const float* __restrict__ expTable,*/ const int* __restrict__ vocab_codelen,
	const char* __restrict__ vocab_code, const int* __restrict__ vocab_point,
	const int* __restrict__ table, long table_size, long vocab_size, volatile float *syn1neg, rand_type *d_random){

	extern __shared__ real s[]; 
		
	real *neu1 = s;
	real *neu1e = s + layer1_size;
	
	real *warpTempReduce = s + 2 * layer1_size + 1;//num_warps total positions after this base address

	for (int sentence_position = blockIdx.x; sentence_position < sentence_length; sentence_position += gridDim.x){
		long long word = sen[sentence_position];
		if (word == -1) continue;
		
		rand_type next_random = d_random[sentence_position];
		next_random = next_random * RAND_MAGIC;

		int b = next_random % window;
		
		for (int c = threadIdx.x; c < layer1_size; c += blockDim.x){
			neu1[c] = 0;
			neu1e[c] = 0;
		}

		// in -> hidden
		int cw = 0;
	
		for (int a = b; a < window * 2 + 1 - b; a++)
			if (a != window) {
				int c = sentence_position - window + a;
				if (c < 0) continue;
				if (c >= sentence_length) continue;
				long long last_word = sen[c];
				if (last_word == -1) continue;

				for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
					neu1[c] += syn0[c + last_word * layer1_size];

				cw++;
			}
		if (cw) {
			for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
				neu1[c] /= cw;

			if (hs){
				for (int d = vocab_codelen[word]; d < vocab_codelen[word + 1]; d++) {
					float f = 0;
					int l2 = vocab_point[d] * layer1_size;

					// Propagate hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1[c + l2];

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);
					if (f <= -MAX_EXP)
						continue;
					else
						if (f >= MAX_EXP)
							continue;
						else
							f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];

					// 'g' is the gradient multiplied by the learning rate
					float g = (1 - vocab_code[d] - f) * alpha;

					// Propagate errors output -> hidden
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1[c + l2];

					// Learn weights hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1[c + l2] += g * neu1[c];
				}
			}
			// NEGATIVE SAMPLING
			if (negative > 0)
				for (int d = 0; d < negative + 1; d++) {
					int target;
					int label;
					if (d == 0) {
						target = word;
						label = 1;
					}
					else {
						
						next_random = next_random * RAND_MAGIC;
													
						target = table[(next_random) % table_size];
						if (target == 0)
							target = next_random % (vocab_size - 1) + 1;

						if (target == word)
							continue;
						label = 0;
					}
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];
					
					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];
				}
			// hidden -> in
			for (int a = b; a < window * 2 + 1 - b; a++)
				if (a != window) {
					int c = sentence_position - window + a;
					if (c < 0) continue;
					if (c >= sentence_length) continue;
					long long last_word = sen[c];
					if (last_word == -1) continue;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn0[c + last_word * layer1_size] += neu1e[c];
				}
		}

		if (0 == threadIdx.x) {
			d_random[sentence_position] = next_random;			
		}
	}
}


template<long long hs>
void __global__ cbow_kernel_block_knnn0(long window, long negative, float alpha, long sentence_length,
	const int* __restrict__ sen, long layer1_size, volatile float *syn0, volatile float *syn1,
	/*const float* __restrict__ expTable,*/ const int* __restrict__ vocab_codelen,
	const char* __restrict__ vocab_code, const int* __restrict__ vocab_point,
	const int* __restrict__ table, long table_size, long vocab_size, volatile float *syn1neg, 
	const int* __restrict__ topks, rand_type* d_random, int K){

	extern __shared__ real s[]; //2*(real *)calloc(layer1_size, sizeof(real));

	real *neu1 = s;
	real *neu1e = s + layer1_size;
	
	real *warpTempReduce = s + 2 * layer1_size + 1;//num_warps total positions after this base address

	for (int sentence_position = blockIdx.x; sentence_position < sentence_length; sentence_position += gridDim.x){
		long long word = sen[sentence_position];
		if (word == -1) continue;
		
		rand_type next_random = d_random[sentence_position];
		next_random = next_random * RAND_MAGIC;

		int b = next_random % window;

		for (int c = threadIdx.x; c < layer1_size; c += blockDim.x){
			neu1[c] = 0;
			neu1e[c] = 0;
		}

		// in -> hidden
		int cw = 0;

		for (int a = b; a < window * 2 + 1 - b; a++)
			if (a != window) {
				int c = sentence_position - window + a;
				if (c < 0) continue;
				if (c >= sentence_length) continue;
				long long last_word = sen[c];
				if (last_word == -1) continue;

				for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
					neu1[c] += syn0[c + last_word * layer1_size];

				cw++;
			}
		if (cw) {
			for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
				neu1[c] /= cw;

			if (hs){
				for (int d = vocab_codelen[word]; d < vocab_codelen[word + 1]; d++) {
					float f = 0;
					int l2 = vocab_point[d] * layer1_size;

					// Propagate hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1[c + l2];

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);
					if (f <= -MAX_EXP)
						continue;
					else
						if (f >= MAX_EXP)
							continue;
						else
							f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];

					// 'g' is the gradient multiplied by the learning rate
					float g = (1 - vocab_code[d] - f) * alpha;

					// Propagate errors output -> hidden
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1[c + l2];

					// Learn weights hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1[c + l2] += g * neu1[c];
				}
			}
			// NEGATIVE SAMPLING
			if (negative > 0){
				int loop = K+1;
				for (int d = 0; d < loop; d++) {
					int target;
					int label;
					if (d == 0) {
						target = word;
						label = 1;
					}
					else {
						//negative equals K
						target = topks[word * K + (d - 1)];

						if (target == word)
							continue;
						label = 0;
					}
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];

					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];
				}
			}
			// hidden -> in
			for (int a = b; a < window * 2 + 1 - b; a++)
				if (a != window) {
					int c = sentence_position - window + a;
					if (c < 0) continue;
					if (c >= sentence_length) continue;
					long long last_word = sen[c];
					if (last_word == -1) continue;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn0[c + last_word * layer1_size] += neu1e[c];
				}
		}
	}
}


template<long long hs>
void __global__ cbow_kernel_block_knnn1(long window, long negative, float alpha, long sentence_length,
	const int* __restrict__ sen, long layer1_size, volatile float *syn0, volatile float *syn1,
	/*const float* __restrict__ expTable,*/ const int* __restrict__ vocab_codelen,
	const char* __restrict__ vocab_code, const int* __restrict__ vocab_point,
	const int* __restrict__ table, long table_size, long vocab_size, volatile float *syn1neg,
	const int* __restrict__ topks, rand_type* d_random, int K){

	extern __shared__ real s[]; //2*(real *)calloc(layer1_size, sizeof(real));

	real *neu1 = s;
	real *neu1e = s + layer1_size;

	real *warpTempReduce = s + 2 * layer1_size + 1;//num_warps total positions after this base address

	for (int sentence_position = blockIdx.x; sentence_position < sentence_length; sentence_position += gridDim.x){
		long long word = sen[sentence_position];
		if (word == -1) continue;

		rand_type next_random = d_random[sentence_position];
		next_random = next_random * RAND_MAGIC;

		int b = next_random % window;

		for (int c = threadIdx.x; c < layer1_size; c += blockDim.x){
			neu1[c] = 0;
			neu1e[c] = 0;
		}

		// in -> hidden
		int cw = 0;

		for (int a = b; a < window * 2 + 1 - b; a++)
			if (a != window) {
				int c = sentence_position - window + a;
				if (c < 0) continue;
				if (c >= sentence_length) continue;
				long long last_word = sen[c];
				if (last_word == -1) continue;

				for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
					neu1[c] += syn0[c + last_word * layer1_size];

				cw++;
			}
		if (cw) {
			for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
				neu1[c] /= cw;

			if (hs){
				for (int d = vocab_codelen[word]; d < vocab_codelen[word + 1]; d++) {
					float f = 0;
					int l2 = vocab_point[d] * layer1_size;

					// Propagate hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1[c + l2];

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);
					if (f <= -MAX_EXP)
						continue;
					else
						if (f >= MAX_EXP)
							continue;
						else
							f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];

					// 'g' is the gradient multiplied by the learning rate
					float g = (1 - vocab_code[d] - f) * alpha;

					// Propagate errors output -> hidden
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1[c + l2];

					// Learn weights hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1[c + l2] += g * neu1[c];
				}
			}
			// NEGATIVE SAMPLING
			if (negative > 0){
				//Calcula normalmente as 'negative' samples com unigram table. Depois faz mais K itera��es com os k vizinhos mais distantes da palavra atual
				//K_iter_start dever� ficar sempre em 1.
				//A ideia desse m�todo � de tentar melhorar o que j� tem, adicionando as K itera��es no final. Provavelmente o K dever� ser pequeno (menor que 'negative').

				int loop = negative + 1;
				int target;
				int label;
				for (int d = 0; d < loop; d++) {					
					if (d == 0) {
						target = word;
						label = 1;
					}
					else {

						next_random = next_random * RAND_MAGIC;

						target = table[(next_random) % table_size];
						if (target == 0)
							target = next_random % (vocab_size - 1) + 1;

						if (target == word)
							continue;
						label = 0;
					
					}
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];

					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];
				}

				//Depois de fazer com as amostras por unigram table, faz mais K com os k vizinhos mais distantes da palavra atual
				label = 0;
				for (int d = 0; d < K; d++) {
					
					target = topks[word * K + d];
				
					if (target == word)
						continue;					
											
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];

					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];
				}
			}
			// hidden -> in
			for (int a = b; a < window * 2 + 1 - b; a++)
				if (a != window) {
					int c = sentence_position - window + a;
					if (c < 0) continue;
					if (c >= sentence_length) continue;
					long long last_word = sen[c];
					if (last_word == -1) continue;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn0[c + last_word * layer1_size] += neu1e[c];
				}
		}
	}
}


template<long long hs>
void __global__ cbow_kernel_block_knnn2(long window, long negative, float alpha, long sentence_length,
	const int* __restrict__ sen, long layer1_size, volatile float *syn0, volatile float *syn1,
	/*const float* __restrict__ expTable,*/ const int* __restrict__ vocab_codelen,
	const char* __restrict__ vocab_code, const int* __restrict__ vocab_point,
	const int* __restrict__ table, long table_size, long vocab_size, volatile float *syn1neg,
	const int* __restrict__ topks, rand_type* d_random, int K){

	extern __shared__ real s[]; //2*(real *)calloc(layer1_size, sizeof(real));

	real *neu1 = s;
	real *neu1e = s + layer1_size;

	real *warpTempReduce = s + 2 * layer1_size + 1;//num_warps total positions after this base address

	for (int sentence_position = blockIdx.x; sentence_position < sentence_length; sentence_position += gridDim.x){
		long long word = sen[sentence_position];
		if (word == -1) continue;

		rand_type next_random = d_random[sentence_position];
		next_random = next_random * RAND_MAGIC;

		int b = next_random % window;

		for (int c = threadIdx.x; c < layer1_size; c += blockDim.x){
			neu1[c] = 0;
			neu1e[c] = 0;
		}

		// in -> hidden
		int cw = 0;

		for (int a = b; a < window * 2 + 1 - b; a++)
			if (a != window) {
				int c = sentence_position - window + a;
				if (c < 0) continue;
				if (c >= sentence_length) continue;
				long long last_word = sen[c];
				if (last_word == -1) continue;

				for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
					neu1[c] += syn0[c + last_word * layer1_size];

				cw++;
			}
		if (cw) {
			for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
				neu1[c] /= cw;

			if (hs){
				for (int d = vocab_codelen[word]; d < vocab_codelen[word + 1]; d++) {
					float f = 0;
					int l2 = vocab_point[d] * layer1_size;

					// Propagate hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1[c + l2];

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);
					if (f <= -MAX_EXP)
						continue;
					else
						if (f >= MAX_EXP)
							continue;
						else
							f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];

					// 'g' is the gradient multiplied by the learning rate
					float g = (1 - vocab_code[d] - f) * alpha;

					// Propagate errors output -> hidden
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1[c + l2];

					// Learn weights hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1[c + l2] += g * neu1[c];
				}
			}
			// NEGATIVE SAMPLING
			if (negative > 0){
				//Como o m�todo 1

				int loop = negative + 1;
				int target;
				int label;
				for (int d = 0; d < loop; d++) {
					if (d == 0) {
						target = word;
						label = 1;
					}
					else {

						next_random = next_random * RAND_MAGIC;

						target = table[(next_random) % table_size];
						if (target == 0)
							target = next_random % (vocab_size - 1) + 1;

						if (target == word)
							continue;
						label = 0;

					}
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];

					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];
				}

				//M�todo 1, mas com pesos no K vizinhos. Peso maior para o primeiro, menor para o segundo, etc..
				label = 0;
				for (int d = 1; d <= K; d++) {

					target = topks[word * K + (d-1)];

					float weight = 1.0 / (expf(d) - 1); // D� mais peso para o 1�, depois reduz
					//float weight = log10(K-(d-1)); // outra tentativa
					label = -weight;

					if (target == word)
						continue;

					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];

					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];
				}
			}
			// hidden -> in
			for (int a = b; a < window * 2 + 1 - b; a++)
				if (a != window) {
					int c = sentence_position - window + a;
					if (c < 0) continue;
					if (c >= sentence_length) continue;
					long long last_word = sen[c];
					if (last_word == -1) continue;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn0[c + last_word * layer1_size] += neu1e[c];
				}
		}
	}
}

template<long long hs>
void __global__ cbow_kernel_block_knnn3(long window, long negative, float alpha, long sentence_length,
	const int* __restrict__ sen, long layer1_size, volatile float *syn0, volatile float *syn1,
	/*const float* __restrict__ expTable,*/ const int* __restrict__ vocab_codelen,
	const char* __restrict__ vocab_code, const int* __restrict__ vocab_point,
	const int* __restrict__ table, long table_size, long vocab_size, volatile float *syn1neg,
	const int* __restrict__ topks, rand_type* d_random, int K){

	extern __shared__ real s[]; //2*(real *)calloc(layer1_size, sizeof(real));

	real *neu1 = s;
	real *neu1e = s + layer1_size;

	real *warpTempReduce = s + 2 * layer1_size + 1;//num_warps total positions after this base address

	for (int sentence_position = blockIdx.x; sentence_position < sentence_length; sentence_position += gridDim.x){
		long long word = sen[sentence_position];
		if (word == -1) continue;

		rand_type next_random = d_random[sentence_position];
		next_random = next_random * RAND_MAGIC;

		int b = next_random % window;

		for (int c = threadIdx.x; c < layer1_size; c += blockDim.x){
			neu1[c] = 0;
			neu1e[c] = 0;
		}

		// in -> hidden
		int cw = 0;

		for (int a = b; a < window * 2 + 1 - b; a++)
			if (a != window) {
				int c = sentence_position - window + a;
				if (c < 0) continue;
				if (c >= sentence_length) continue;
				long long last_word = sen[c];
				if (last_word == -1) continue;

				for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
					neu1[c] += syn0[c + last_word * layer1_size];

				cw++;
			}
		if (cw) {
			for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
				neu1[c] /= cw;

			if (hs){
				for (int d = vocab_codelen[word]; d < vocab_codelen[word + 1]; d++) {
					float f = 0;
					int l2 = vocab_point[d] * layer1_size;

					// Propagate hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1[c + l2];

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);
					if (f <= -MAX_EXP)
						continue;
					else
						if (f >= MAX_EXP)
							continue;
						else
							f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];

					// 'g' is the gradient multiplied by the learning rate
					float g = (1 - vocab_code[d] - f) * alpha;

					// Propagate errors output -> hidden
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1[c + l2];

					// Learn weights hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1[c + l2] += g * neu1[c];
				}
			}
			// NEGATIVE SAMPLING
			if (negative > 0){
				//Tenta usar os k mais pr�ximos como amostras positivas tamb�m. Com label (weight) ponderado menor que 1
				//Logo ap�s fazer a amostra positiva com d==0 (target = word), faz K outras parcialmente positivas.

				int loop = negative + 1;
				int target;
				int label;
				for (int d = 0; d < loop; d++) {
					if (d == 0) {
						target = word;
						label = 1;
					}
					else {

						next_random = next_random * RAND_MAGIC;

						target = table[(next_random) % table_size];
						if (target == 0)
							target = next_random % (vocab_size - 1) + 1;

						if (target == word)
							continue;
						label = 0;

					}
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];

					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];

					//Tenta usar os k mais pr�ximos como amostras positivas tamb�m. Com label (weight) menor que 1
					if (d == 0)
						for (int d = 1; d <= K; d++) {

							target = topks[word * K + (d - 1)];

							float weight = 1.0 / (expf(d) - 1); // D� mais peso para o 1�, depois reduz
							//float weight = log10(K-(d-1)); // outra tentativa
							label = weight;

							if (target == word)
								continue;

							int l2 = target * layer1_size;
							float f = 0;
							float g;
							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								f += neu1[c] * syn1neg[c + l2];

							__syncthreads();

							f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

							__syncthreads();

							if (f > MAX_EXP)
								g = (label - 1) * alpha;
							else
								if (f < -MAX_EXP)
									g = (label - 0) * alpha;
								else
									g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								neu1e[c] += g * syn1neg[c + l2];

							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								syn1neg[c + l2] += g * neu1[c];
						}
				}

		
			}
			// hidden -> in
			for (int a = b; a < window * 2 + 1 - b; a++)
				if (a != window) {
					int c = sentence_position - window + a;
					if (c < 0) continue;
					if (c >= sentence_length) continue;
					long long last_word = sen[c];
					if (last_word == -1) continue;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn0[c + last_word * layer1_size] += neu1e[c];
				}
		}
	}
}

template<long long hs>
void __global__ cbow_kernel_block_knnn4(long window, long negative, float alpha, long sentence_length,
	const int* __restrict__ sen, long layer1_size, volatile float *syn0, volatile float *syn1,
	/*const float* __restrict__ expTable,*/ const int* __restrict__ vocab_codelen,
	const char* __restrict__ vocab_code, const int* __restrict__ vocab_point,
	const int* __restrict__ table, long table_size, long vocab_size, volatile float *syn1neg,
	const int* __restrict__ topks, rand_type* d_random, int K){

	extern __shared__ real s[]; //2*(real *)calloc(layer1_size, sizeof(real));

	real *neu1 = s;
	real *neu1e = s + layer1_size;

	real *warpTempReduce = s + 2 * layer1_size + 1;//num_warps total positions after this base address

	for (int sentence_position = blockIdx.x; sentence_position < sentence_length; sentence_position += gridDim.x){
		long long word = sen[sentence_position];
		if (word == -1) continue;

		rand_type next_random = d_random[sentence_position];
		next_random = next_random * RAND_MAGIC;

		int b = next_random % window;

		for (int c = threadIdx.x; c < layer1_size; c += blockDim.x){
			neu1[c] = 0;
			neu1e[c] = 0;
		}

		// in -> hidden
		int cw = 0;

		for (int a = b; a < window * 2 + 1 - b; a++)
			if (a != window) {
				int c = sentence_position - window + a;
				if (c < 0) continue;
				if (c >= sentence_length) continue;
				long long last_word = sen[c];
				if (last_word == -1) continue;

				for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
					neu1[c] += syn0[c + last_word * layer1_size];

				cw++;
			}
		if (cw) {
			for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
				neu1[c] /= cw;

			if (hs){
				for (int d = vocab_codelen[word]; d < vocab_codelen[word + 1]; d++) {
					float f = 0;
					int l2 = vocab_point[d] * layer1_size;

					// Propagate hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1[c + l2];

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);
					if (f <= -MAX_EXP)
						continue;
					else
						if (f >= MAX_EXP)
							continue;
						else
							f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];

					// 'g' is the gradient multiplied by the learning rate
					float g = (1 - vocab_code[d] - f) * alpha;

					// Propagate errors output -> hidden
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1[c + l2];

					// Learn weights hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1[c + l2] += g * neu1[c];
				}
			}
			// NEGATIVE SAMPLING
			if (negative > 0){
				//Tenta usar os k mais pr�ximos como amostras adicionais para cada amostra do unigram table. 
				//Basicamente multiplica as amostras negativas por K

				int loop = negative + 1;
				int target;
				int label;
				for (int d = 0; d < loop; d++) {
					if (d == 0) {
						target = word;
						label = 1;
					}
					else {

						next_random = next_random * RAND_MAGIC;

						target = table[(next_random) % table_size];
						if (target == 0)
							target = next_random % (vocab_size - 1) + 1;

						if (target == word)
							continue;
						label = 0;

					}
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];

					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];

					//Tenta usar os k mais pr�ximos da palavra amostrada como negativa tamb�m
					if (d > 0)
						for (int k = 1; k <= K; k++) {

							int ntarget = topks[target * K + (k - 1)];
							label = 0;

							if (ntarget == word || ntarget == 0)
								continue;

							int l2 = ntarget * layer1_size;
							float f = 0;
							float g;
							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								f += neu1[c] * syn1neg[c + l2];

							__syncthreads();

							f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

							__syncthreads();

							if (f > MAX_EXP)
								g = (label - 1) * alpha;
							else
								if (f < -MAX_EXP)
									g = (label - 0) * alpha;
								else
									g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								neu1e[c] += g * syn1neg[c + l2];

							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								syn1neg[c + l2] += g * neu1[c];
						}
				}


			}
			// hidden -> in
			for (int a = b; a < window * 2 + 1 - b; a++)
				if (a != window) {
					int c = sentence_position - window + a;
					if (c < 0) continue;
					if (c >= sentence_length) continue;
					long long last_word = sen[c];
					if (last_word == -1) continue;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn0[c + last_word * layer1_size] += neu1e[c];
				}
		}
	}
}


template<long long hs>
void __global__ cbow_kernel_block_knnn5(long window, long negative, float alpha, long sentence_length,
	const int* __restrict__ sen, long layer1_size, volatile float *syn0, volatile float *syn1,
	/*const float* __restrict__ expTable,*/ const int* __restrict__ vocab_codelen,
	const char* __restrict__ vocab_code, const int* __restrict__ vocab_point,
	const int* __restrict__ table, long table_size, long vocab_size, volatile float *syn1neg,
	const int* __restrict__ topks, rand_type* d_random, int K){

	extern __shared__ real s[]; //2*(real *)calloc(layer1_size, sizeof(real));

	real *neu1 = s;
	real *neu1e = s + layer1_size;

	real *warpTempReduce = s + 2 * layer1_size + 1;//num_warps total positions after this base address

	for (int sentence_position = blockIdx.x; sentence_position < sentence_length; sentence_position += gridDim.x){
		long long word = sen[sentence_position];
		if (word == -1) continue;

		rand_type next_random = d_random[sentence_position];
		next_random = next_random * RAND_MAGIC;

		int b = next_random % window;

		for (int c = threadIdx.x; c < layer1_size; c += blockDim.x){
			neu1[c] = 0;
			neu1e[c] = 0;
		}

		// in -> hidden
		int cw = 0;

		for (int a = b; a < window * 2 + 1 - b; a++)
			if (a != window) {
				int c = sentence_position - window + a;
				if (c < 0) continue;
				if (c >= sentence_length) continue;
				long long last_word = sen[c];
				if (last_word == -1) continue;

				for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
					neu1[c] += syn0[c + last_word * layer1_size];

				cw++;
			}
		if (cw) {
			for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
				neu1[c] /= cw;

			if (hs){
				for (int d = vocab_codelen[word]; d < vocab_codelen[word + 1]; d++) {
					float f = 0;
					int l2 = vocab_point[d] * layer1_size;

					// Propagate hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1[c + l2];

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);
					if (f <= -MAX_EXP)
						continue;
					else
						if (f >= MAX_EXP)
							continue;
						else
							f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];

					// 'g' is the gradient multiplied by the learning rate
					float g = (1 - vocab_code[d] - f) * alpha;

					// Propagate errors output -> hidden
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1[c + l2];

					// Learn weights hidden -> output
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1[c + l2] += g * neu1[c];
				}
			}
			// NEGATIVE SAMPLING
			if (negative > 0){
				//M�todo 4 mas com pondera��o 

				int loop = negative + 1;
				int target;
				int label;
				for (int d = 0; d < loop; d++) {
					if (d == 0) {
						target = word;
						label = 1;
					}
					else {

						next_random = next_random * RAND_MAGIC;

						target = table[(next_random) % table_size];
						if (target == 0)
							target = next_random % (vocab_size - 1) + 1;

						if (target == word)
							continue;
						label = 0;

					}
					int l2 = target * layer1_size;
					float f = 0;
					float g;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						f += neu1[c] * syn1neg[c + l2];

					__syncthreads();

					f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

					__syncthreads();

					if (f > MAX_EXP)
						g = (label - 1) * alpha;
					else
						if (f < -MAX_EXP)
							g = (label - 0) * alpha;
						else
							g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						neu1e[c] += g * syn1neg[c + l2];

					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn1neg[c + l2] += g * neu1[c];

					//Tenta usar os k mais pr�ximos da palavra amostrada como negativa tamb�m
					if (d > 0)
						for (int k = 1; k <= K; k++) {

							int ntarget = topks[target * K + (k - 1)];
							float weight = 1.0 / (expf(k) - 1); // D� mais peso para o 1�, depois reduz sucessivamente
							//float weight = log10(K-(k-1)); // outra tentativa
							label = -weight;

							if (ntarget == word || ntarget == 0)
								continue;

							int l2 = ntarget * layer1_size;
							float f = 0;
							float g;
							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								f += neu1[c] * syn1neg[c + l2];

							__syncthreads();

							f = reduceInBlock(f, blockDim.x / warpSize, warpTempReduce);

							__syncthreads();

							if (f > MAX_EXP)
								g = (label - 1) * alpha;
							else
								if (f < -MAX_EXP)
									g = (label - 0) * alpha;
								else
									g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;

							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								neu1e[c] += g * syn1neg[c + l2];

							for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
								syn1neg[c + l2] += g * neu1[c];
						}
				}


			}
			// hidden -> in
			for (int a = b; a < window * 2 + 1 - b; a++)
				if (a != window) {
					int c = sentence_position - window + a;
					if (c < 0) continue;
					if (c >= sentence_length) continue;
					long long last_word = sen[c];
					if (last_word == -1) continue;
					for (int c = threadIdx.x; c < layer1_size; c += blockDim.x)
						syn0[c + last_word * layer1_size] += neu1e[c];
				}
		}
	}
}
void __global__ l2norm_calc_kernel(long layer1_size, float *syn0, long vocab_size, float *l2norms){

	int numWarpsPerBlock = blockDim.x / warpSize;
	int warpIdInBlock = threadIdx.x / warpSize;
	int warpId = warpIdInBlock + numWarpsPerBlock*blockIdx.x;
	int idInWarp = threadIdx.x % warpSize;

	//First word is just </s>
	for (int word = warpId + 1; word < vocab_size; word += gridDim.x * numWarpsPerBlock){
			
		float norm = 0;
		for (int i = idInWarp; i < layer1_size; i += warpSize){
			float tmp = syn0[word * layer1_size + i];
			norm += tmp * tmp;
		}
		norm = reduceInWarp(norm, 0);

		if (!idInWarp)
			l2norms[word] = sqrtf(norm);
	}
}

//calculates cosine distance of each word in relation to the target word. One warp per word. target_word norm is not needed in the division
void __global__ cosine_distance_calc_kernel(long target_word, long layer1_size, float *syn0, long vocab_size, float *l2norms, Similarity *dists){

	int numWarpsPerBlock = blockDim.x / warpSize;
	int warpIdInBlock = threadIdx.x / warpSize;
	int warpId = warpIdInBlock + numWarpsPerBlock*blockIdx.x;
	int idInWarp = threadIdx.x % warpSize;
	
	//First word is just </s>
	for (int word = warpId + 1; word < vocab_size; word += gridDim.x * numWarpsPerBlock){

		if (!idInWarp){
			dists[word].word = word;		
		}
		float tmp = 0;
		for (int i = idInWarp; i < layer1_size; i += warpSize){
			tmp += syn0[word * layer1_size + i] * syn0[target_word * layer1_size + i];			
		}
		
		tmp = reduceInWarp(tmp, 0);

		if (!idInWarp){
			float norm = l2norms[word];
			if( norm > 0)
				dists[word].distance = tmp /norm ; 
		}
	}
	//First word is just </s>
	if (!threadIdx.x && !blockIdx.x)
		dists[0].distance = 1111111111;
}


void __global__ write_topk_kernel(long word, int K, Similarity *dists, int* topks){

	if (threadIdx.x < K){
		topks[word * K + threadIdx.x] = dists[threadIdx.x + 1].word;
	}	
}

void cbow_cuda(long window, long negative, float alpha, long sentence_length, int *sen,
	long layer1_size, float *syn0, long hs, float *syn1, float *expTable, int *vocab_codelen,
	char *vocab_code, int *vocab_point, int *table, long table_size, long vocab_size, float *syn1neg, rand_type* d_random){
	
	if (useWarpApproach){
		int gridSize = max((sentence_length) / (blockSize / 32), 1L);
		size_t smsize = (blockSize / 32)*(2 * layer1_size + 3)*sizeof(float); // +3 before
		//printf("sm size is %d\n", smsize);
		//fflush(stdout);
		if (hs)
			cbow_kernel<1> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg);
		else
			cbow_kernel<0> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg);
	}
	else{
		int gridSize = max(sentence_length,1L);
		size_t smsize = (2 * layer1_size + 3 + blockSize / 32)*sizeof(float);
		//printf("sm size is %d\n", smsize);
		//fflush(stdout);	
		if (hs)
			cbow_kernel_block<1> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, d_random);
		else
			cbow_kernel_block<0> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, d_random);
	}
}

void cbow_cuda_knnn(long window, long negative, float alpha, long sentence_length, int *sen,
	long layer1_size, float *syn0, long hs, float *syn1, float *expTable, int *vocab_codelen,
	char *vocab_code, int *vocab_point, int *table, long table_size, long vocab_size,
	float *syn1neg, int* topks, rand_type *d_random, int K, int method){

		
	int gridSize = max(sentence_length, 1L);
	size_t smsize = (2 * layer1_size  + blockSize / 32)*sizeof(float);
	//printf("sm size is %d\n", smsize);
	//fflush(stdout);	
	switch (method){
		//method 0 is the best. KFN as negative samples
	case 0:
		if (hs)
			cbow_kernel_block_knnn0<1> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		else
			cbow_kernel_block_knnn0<0> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		break;
	case 1:
		if (hs)
			cbow_kernel_block_knnn1<1> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		else
			cbow_kernel_block_knnn1<0> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		break;
	case 2:
		if (hs)
			cbow_kernel_block_knnn2<1> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		else
			cbow_kernel_block_knnn2<0> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		break;
	case 3:
		if (hs)
			cbow_kernel_block_knnn3<1> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		else
			cbow_kernel_block_knnn3<0> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		break;
	case 4:
		if (hs)
			cbow_kernel_block_knnn4<1> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		else
			cbow_kernel_block_knnn4<0> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		break;
	case 5:
		if (hs)
			cbow_kernel_block_knnn5<1> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		else
			cbow_kernel_block_knnn5<0> << <gridSize, blockSize, smsize >> >(window, negative, alpha, sentence_length,
			sen, layer1_size, syn0, syn1, /*expTable,*/ vocab_codelen, vocab_code, vocab_point, table, table_size, vocab_size, syn1neg, topks, d_random, K);
		break;
	}
	
}
