#include<bits/stdc++.h>
#include "fnv1a.h"
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/unique.h>
#include <thrust/binary_search.h>
#include <thrust/adjacent_difference.h>


using namespace std;
hash<string> hasher;

const unsigned int N = 5*1e7 / 2;
const unsigned int C = N*4 + 1e5;

uint64_t *word_hash[32];
int pos[32];
int sz[32];

uint64_t* gpuWords;
uint64_t* gpuPair1;
uint64_t* gpuPair2;


thrust::device_vector<uint64_t> gpuUniquePair1;
thrust::device_vector<uint64_t> gpuUniquePair2;
thrust::device_vector<uint64_t> gpuCount;

thrust::host_vector<uint64_t> cpuUniquePair1[32];
thrust::host_vector<uint64_t> cpuUniquePair2[32];
thrust::host_vector<uint64_t> cpuCount[32];

const uint8_t MAX_WORD_LENGTH = 30;
char *GetNextWord (char *text, char *token)
{
    bool quote_found = false;
    bool hyphen_found = false;

    uint8_t char_index = 0;


    while (*text)
    {
        char c = *text++;

        if (isalpha (c))
        {
            c = static_cast<char>(tolower (c));

            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }
        }
        else if ((c == '\'') && (!quote_found) && (char_index > 0))
        {
            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }

            quote_found = true;
        }
        else if ((c == '-') && (!hyphen_found) && (char_index > 0))
        {
            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }

            hyphen_found = true;
        }
        else if (char_index > 0)
        {
            //
            // Normaliza o índice para evitar que a string passe do limite do buffer.
            //
            char_index = (uint8_t) ((char_index > MAX_WORD_LENGTH - 1) ? MAX_WORD_LENGTH - 1 : char_index);

            //
            // Descarta o apóstrofo ou o hífen se este estiver isolado no final do token.
            //
            // Ex.: 'String' -> String'
            //      'Window' -> Window'
            //
            if ((token[char_index - 1] == '\'') || (token[char_index - 1] == '-'))
            {
                --char_index;
            }

            break;
        } else break;
    }

    char_index = min(char_index, MAX_WORD_LENGTH);
    token[char_index] = '\0';

    return (*text) ? text : nullptr;
}

// --- Defining tuple type
typedef thrust::tuple<uint64_t, uint64_t> Tuple;

/**************************/
/* TUPLE ORDERING FUNCTOR */
/**************************/
struct TupleComp
{
    __host__ __device__ bool operator()(const Tuple& t1, const Tuple& t2)
    {
        if (t1.get<0>() < t2.get<0>())
            return true;
        if (t1.get<0>() > t2.get<0>())
            return false;
        return t1.get<1>() < t2.get<1>();
    }
};
struct tupleEqual
{
    __host__ __device__
        bool operator()(const Tuple& x, const Tuple& y)
    {
        return ((x.get<0>() == y.get<0>()) && (x.get<1>() == y.get<1>()));
    }
};
__global__
void copy_with_offset(int n, uint64_t *words, uint64_t *v, int offset)
{
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  
  if(index + offset < n && index + offset >= 0){
    v[index] = words[index+offset];
  } else v[index] = 0;
}

void eliminate_useless_memory(
        int i, 
        thrust::host_vector<uint64_t> &cpuUniquePair1, 
        thrust::host_vector<uint64_t> &cpuUniquePair2, 
        thrust::host_vector<uint64_t> &cpuCount){
    int j;
    int T = cpuUniquePair1.size();
    for(j = 0; j < T && cpuUniquePair1[j] != ULLONG_MAX; j++);
    cout << "Arrays " << i << " with size: " << j << ", factor: " << (double)j / T << endl;
    cpuUniquePair1.resize(j);
    cpuUniquePair2.resize(j);
    cpuCount.resize(j);
}

int main(){
    ios::sync_with_stdio(0);
    cin.tie(0);

    char word[50];
    char aux[50];
    
    int i = 0, cnt = 0;
    int K = 0;

    word_hash[0] = (uint64_t*)malloc(sizeof(uint64_t)*N);
    while(cin >> word[0]){
        if(word[0] == '\0') continue;

        GetNextWord(word, aux);
        if(strlen(aux) <= 2) continue;
        word_hash[K][i] = fnv1a(aux);
          
        // ----- LOG -------
        i++;
        cnt++;
        if(i == N){
            cout << cnt << endl;
            i = 0;
            K++;
            word_hash[K] = (uint64_t*)malloc(sizeof(uint64_t) * N);
        }
        // -----------------
    }
    K++;
    cudaMalloc(&gpuWords, sizeof(uint64_t) * N);
    cudaMalloc(&gpuPair1, sizeof(uint64_t) * C);
    cudaMalloc(&gpuPair2, sizeof(uint64_t) * C);


    int num_unique_pair = N*3/2;
    gpuUniquePair1 = thrust::device_vector<uint64_t>(num_unique_pair);
    gpuUniquePair2 = thrust::device_vector<uint64_t>(num_unique_pair);
    gpuCount = thrust::device_vector<uint64_t>(num_unique_pair);

    vector<std::thread> threads;

    for(i = 0; i < K; i++){
        int number_words = (i == K-1 ? cnt % N : N);
        int T = number_words*4 - 6;
        if(T < 0) continue;

        cudaMemcpy(gpuWords, word_hash[i], sizeof(uint64_t)*number_words, cudaMemcpyHostToDevice);
        auto free_word_hash = [](uint64_t *word_hash){
            free(word_hash);
        };
        threads.push_back(std::thread(free_word_hash, word_hash[i]));
        for(int d = -2; d <= 2; d++){
            if(d == 0) continue;
            int offset = d < 0 ? (d+2)*number_words : (d+1)*number_words;
            cout << "Offset: " << offset << endl;

            int blockSize = 1024;
            int numBlocks = (number_words + blockSize - 1) / blockSize;
            copy_with_offset<<<numBlocks, blockSize>>>(number_words, gpuWords, gpuPair1 + offset, 0);
            copy_with_offset<<<numBlocks, blockSize>>>(number_words, gpuWords, gpuPair2 + offset, d);
        }

        thrust::fill(gpuUniquePair1.begin(), gpuUniquePair1.end(), ULLONG_MAX);
        thrust::fill(gpuUniquePair2.begin(), gpuUniquePair2.end(), ULLONG_MAX);
        thrust::fill(gpuCount.begin(), gpuCount.end(), 0);

        cout << "Calculou pairs -----------" << endl;
        // Ordenar vetor gpu Pair
        // --- From raw pointers to device_ptr
        thrust::device_ptr<uint64_t> dev_ptr_pair1 = thrust::device_pointer_cast(gpuPair1);
        thrust::device_ptr<uint64_t> dev_ptr_pair2 = thrust::device_pointer_cast(gpuPair2);
        

        auto begin = thrust::make_zip_iterator(thrust::make_tuple(dev_ptr_pair1, dev_ptr_pair2));
        auto end = thrust::make_zip_iterator(thrust::make_tuple(dev_ptr_pair1 + T, dev_ptr_pair2 + T));

        cout << "Antes do sort" << endl;
        thrust::sort(begin, end, TupleComp());
        cout << "Fez sort ----------" << endl;


        auto begin_unique = thrust::make_zip_iterator(thrust::make_tuple(gpuUniquePair1.begin(), gpuUniquePair2.begin()));

        thrust::unique_copy(begin, end, begin_unique, tupleEqual());

        auto end_unique = thrust::make_zip_iterator(thrust::make_tuple(gpuUniquePair1.end(), gpuUniquePair2.end()));
        thrust::upper_bound(begin, end, begin_unique, end_unique, gpuCount.begin(), TupleComp()); 

        cpuUniquePair1[i] = gpuUniquePair1;
        cpuUniquePair2[i] = gpuUniquePair2;
        cpuCount[i] = gpuCount;

        threads.push_back(std::thread(eliminate_useless_memory, 
                                      i, 
                                      std::ref(cpuUniquePair1[i]), 
                                      std::ref(cpuUniquePair2[i]),
                                      std::ref(cpuCount[i]))
        );
    }

    for(auto &u : threads)
        u.join();

    priority_queue< 
        pair<pair<uint64_t, uint64_t>, pair<uint64_t, int> >
        > pq;
    
    for(i = 0; i < K; i++){
        pq.push({{cpuUniquePair1[i].back(), cpuUniquePair2[i].back()}, {cpuCount[i].back(), i}});
        cpuUniquePair1[i].pop_back();
        cpuUniquePair2[i].pop_back();
        cpuCount[i].pop_back();
    }

    cout << "Calculando resultado final" << endl;
    queue<pair<pair<uint64_t, uint64_t>, uint64_t> > q;
    uint64_t elem1 = 0, elem2;
    int count;
    while(!pq.empty()){
        auto &u = pq.top();
        if(elem1 != u.first.first || elem2 != u.first.second){
            if(elem1 != 0 && elem2 != 0){
                q.push({{elem1, elem2}, count});
            }
            elem1 = u.first.first;
            elem2 = u.first.second;
            count = u.second.first;
        } else {
            count += u.second.first;
        }
        i = u.second.second;
        pq.pop();
        
        if(cpuUniquePair1[i].size() > 0){
            pq.push({{cpuUniquePair1[i].back(), cpuUniquePair2[i].back()}, {cpuCount[i].back(), i}});
            cpuUniquePair1[i].pop_back();
            cpuUniquePair2[i].pop_back();
            cpuCount[i].pop_back();
        }
    }
    
    cout << "Imprimir resultado" << endl;
    ofstream out;
    out.open("result.txt", ofstream::out);
    while(!q.empty()){
        auto &u = q.front();
        out << u.first.first << " " << u.first.second << " " << u.second << "\n";
        q.pop();
    }    
    out.close();
    // Imprimir resultado
    
    cudaFree(gpuWords);
    cudaFree(gpuPair1);
    cudaFree(gpuPair2);

    return 0;   
}

