/**
 * Definições relativas ao algoritmo de 'hashing' usado na implementação.
 */

#ifndef __FNV1A_H_f7985100_c28c_4051_a067_9e64163dc917__
#define __FNV1A_H_f7985100_c28c_4051_a067_9e64163dc917__


#include <cstdint>



#define FNV_OFFSET_BASIS UINT64_C(0xcbf29ce484222325)
#define FNV_PRIME        UINT64_C(1099511628211)


/**
 * Efetua um ciclo do hash (não criptográfico) em um caractere qualquer.
 *
 * Referência: https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
 *
 * @param hash Hash criptográfico previamente inicializado.
 * @param c Caractere que será usado para atualizar o hash.
 * @return
 */
inline uint64_t fnv1a_partial (uint64_t hash, char c)
{
    uint8_t byte = (uint8_t) c;

    hash ^= byte;
    hash *= FNV_PRIME;

    return hash;
}



/**
 * Efetua o hash (não criptográfico) de uma palavra qualquer.
 *
 * Referência: https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
 *
 * @param word Palavra que será analisada.
 * @return
 */
inline uint64_t fnv1a (const char *word)
{
    uint64_t hash = FNV_OFFSET_BASIS;
    uint8_t byte;

    while (*word)
    {
        byte = (uint8_t) *word;

        hash ^= byte;
        hash *= FNV_PRIME;

        ++word;
    }

    return hash;
}



#endif // __FNV1A_H_f7985100_c28c_4051_a067_9e64163dc917__
