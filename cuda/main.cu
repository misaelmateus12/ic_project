#include<bits/stdc++.h>
#include "fnv1a.h"
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/unique.h>
#include <thrust/binary_search.h>
#include <thrust/adjacent_difference.h>


using namespace std;

#define TSIZE 1048576
#define SEED 1159241

#define HASHFN bitwisehash
const unsigned int device_memory_capacity = 15*1e8;
const int window_size = 4;

uint32_t *word_hash[32];
uint32_t *word_id[32];
uint32_t *vocab;

int pos[32];
int sz[32];

uint32_t* gpuWords;
uint32_t* gpuPair1;
uint32_t* gpuPair2;


thrust::device_vector<uint32_t> gpuUniquePair1;
thrust::device_vector<uint32_t> gpuUniquePair2;
thrust::device_vector<uint32_t> gpuCount;

thrust::host_vector<uint32_t> cpuUniquePair1[32];
thrust::host_vector<uint32_t> cpuUniquePair2[32];
thrust::host_vector<uint32_t> cpuCount[32];

#define MAX_STRING_LENGTH 30
int get_word(char *word, FILE *fin) {
    int i = 0, ch;
    for ( ; ; ) {
        ch = fgetc(fin);
        if (ch == '\r') continue;
        if (i == 0 && ((ch == '\n') || (ch == EOF))) {
            word[i] = 0;
            return 1;
        }
        if (i == 0 && ((ch == ' ') || (ch == '\t'))) continue; // skip leading space
        if ((ch == EOF) || (ch == ' ') || (ch == '\t') || (ch == '\n')) {
            if (ch == '\n') ungetc(ch, fin); // return the newline next time as document ender
            break;
        }
        if (i < MAX_STRING_LENGTH - 1)
          word[i++] = ch; // don't allow words to exceed MAX_STRING_LENGTH
    }
    word[i] = 0; //null terminate
    // avoid truncation destroying a multibyte UTF-8 char except if only thing on line (so the i > x tests won't overwrite word[0])
    // see https://en.wikipedia.org/wiki/UTF-8#Description
    if (i == MAX_STRING_LENGTH - 1 && (word[i-1] & 0x80) == 0x80) {
        if ((word[i-1] & 0xC0) == 0xC0) {
            word[i-1] = '\0';
        } else if (i > 2 && (word[i-2] & 0xE0) == 0xE0) {
            word[i-2] = '\0';
        } else if (i > 3 && (word[i-3] & 0xF8) == 0xF0) {
            word[i-3] = '\0';
        }
    }
    return 0;
}

/* Efficient string comparison */
int scmp( char *s1, char *s2 ) {
    while (*s1 != '\0' && *s1 == *s2) {s1++; s2++;}
    return(*s1 - *s2);
}

typedef struct hashrec {
    char        *word;
    long long id;
    struct hashrec *next;
} HASHREC;

/* Simple bitwise hash function */
unsigned int bitwisehash(char *word, int tsize, unsigned int seed) {
    char c;
    unsigned int h;
    h = seed;
    for (; (c =* word) != '\0'; word++) h ^= ((h << 5) + c + (h >> 2));
    return((unsigned int)((h&0x7fffffff) % tsize));
}


/* Create hash table, initialise pointers to NULL */
HASHREC ** inithashtable() {
    int i;
    HASHREC **ht;
    ht = (HASHREC **) malloc( sizeof(HASHREC *) * TSIZE );
    for (i = 0; i < TSIZE; i++) ht[i] = (HASHREC *) NULL;
    return(ht);
}

/* Search hash table for given string, return record if found, else NULL */
HASHREC *hashsearch(HASHREC **ht, char *w) {
    HASHREC     *htmp, *hprv;
    unsigned int hval = HASHFN(w, TSIZE, SEED);
    for (hprv = NULL, htmp=ht[hval]; htmp != NULL && scmp(htmp->word, w) != 0; hprv = htmp, htmp = htmp->next);
    if ( htmp != NULL && hprv!=NULL ) { // move to front on access
        hprv->next = htmp->next;
        htmp->next = ht[hval];
        ht[hval] = htmp;
    }
    return(htmp);
}

/* Insert string in hash table, check for duplicates which should be absent */
void hashinsert(HASHREC **ht, char *w, long long id) {
    HASHREC     *htmp, *hprv;
    unsigned int hval = HASHFN(w, TSIZE, SEED);
    for (hprv = NULL, htmp = ht[hval]; htmp != NULL && scmp(htmp->word, w) != 0; hprv = htmp, htmp = htmp->next);
    if (htmp == NULL) {
        htmp = (HASHREC *) malloc(sizeof(HASHREC));
        htmp->word = (char *) malloc(strlen(w) + 1);
        strcpy(htmp->word, w);
        htmp->id = id;
        htmp->next = NULL;
        if (hprv == NULL) ht[hval] = htmp;
        else hprv->next = htmp;
    }
    else fprintf(stderr, "Error, duplicate entry located: %s %s %lld.\n",htmp->word, w, id);
    return;
}


// --- Defining tuple type
typedef thrust::tuple<uint32_t, uint32_t> Tuple;

/**************************/
/* TUPLE ORDERING FUNCTOR */
/**************************/
struct TupleComp
{
    __host__ __device__ bool operator()(const Tuple& t1, const Tuple& t2)
    {
        if (t1.get<0>() < t2.get<0>())
            return true;
        if (t1.get<0>() > t2.get<0>())
            return false;
        return t1.get<1>() < t2.get<1>();
    }
};
struct tupleEqual
{
    __host__ __device__
        bool operator()(const Tuple& x, const Tuple& y)
    {
        return ((x.get<0>() == y.get<0>()) && (x.get<1>() == y.get<1>()));
    }
};
__global__
void copy_with_offset(int n, uint32_t *words, uint32_t *v, int offset)
{
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  
  if(index + offset < n && index + offset >= 0){
    v[index] = words[index+offset];
  } else v[index] = 0;
}

void eliminate_useless_memory(
        int i, 
        thrust::host_vector<uint32_t> &cpuUniquePair1, 
        thrust::host_vector<uint32_t> &cpuUniquePair2, 
        thrust::host_vector<uint32_t> &cpuCount){
    int j;
    int T = cpuUniquePair1.size();
    for(j = 0; j < T && cpuUniquePair1[j] != UINT_MAX; j++);
    cout << "Arrays " << i << " with size: " << j << ", factor: " << (double)j / T << endl;
    cpuUniquePair1.resize(j);
    cpuUniquePair2.resize(j);
    cpuCount.resize(j);
}

int main(){
    ios::sync_with_stdio(0);
    cin.tie(0);
    int total_time = 0;    
    
    FILE *fid;

    cout << "Ler vocabulario" << endl;
    uint64_t id;
    HASHREC *htmp, **vocab_hash = inithashtable();
    char vocab_file[MAX_STRING_LENGTH] = "vocab.txt";
    char format[20], str[MAX_STRING_LENGTH+1];
    sprintf(format,"%%%ds %%lld", MAX_STRING_LENGTH); // Format to read from vocab file, which has (irrelevant) frequency data
    int j = 0;
    fid = fopen(vocab_file,"r");
    if (fid == NULL) {printf("Unable to open vocab file %s.\n",vocab_file); return 1;}
    while (fscanf(fid, format, str, &id) != EOF) hashinsert(vocab_hash, str, ++j);

    cout << "Fim leitura de vocabulario\n" << endl;

    char word[MAX_STRING_LENGTH];
    fid = stdin;    
    int i = 0, cnt = 0;
    int K = 0;

    int N = device_memory_capacity/(8*window_size + 5);
    int C = N*window_size*2 + 1e5;
    cout << "N = " << N << endl;
    word_hash[0] = (uint32_t*)malloc(sizeof(uint32_t)*N);
    
    while(1){
        int flag = get_word(word, fid);
        
        if(flag == 1 && feof(fid)){
            break;
        }
        if(word[0] == '\0') continue;
        if(strlen(word) <= 2) continue;
        
        htmp = hashsearch(vocab_hash, word);
        if(htmp == NULL) continue;
        
        word_hash[K][i] = htmp->id;
        
        // ----- LOG -------
        i++;
        cnt++;
        if(i == N){
            cout << cnt << endl;
            i = 0;
            K++;
            word_hash[K] = (uint32_t*)malloc(sizeof(uint32_t) * N);
        }
        // -----------------
    }
    K++;
    cout << "Terminou leitura" << endl;

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    cudaMalloc(&gpuWords, sizeof(uint32_t) * N);
    cudaMalloc(&gpuPair1, sizeof(uint32_t) * C);
    cudaMalloc(&gpuPair2, sizeof(uint32_t) * C);

    cout << "Terminou alocação memoria GPU" << endl;

    int num_unique_pair = N;
    gpuUniquePair1 = thrust::device_vector<uint32_t>(num_unique_pair);
    gpuUniquePair2 = thrust::device_vector<uint32_t>(num_unique_pair);
    gpuCount = thrust::device_vector<uint32_t>(num_unique_pair);

    cout << "Terminou alocacao memoria GPU Thrust" << endl;
    vector<std::thread> threads;

    for(i = 0; i < K; i++){
        int number_words = (i == K-1 ? cnt % N : N);
        int T = number_words*4 - 6;
        if(T < 0) continue;

        cudaMemcpy(gpuWords, word_hash[i], sizeof(uint32_t)*number_words, cudaMemcpyHostToDevice);
        auto free_word_hash = [](uint32_t *word_hash){
            free(word_hash);
        };
        threads.push_back(std::thread(free_word_hash, word_hash[i]));
        for(int d = -window_size; d <= window_size; d++){
            if(d == 0) continue;
            int offset = d < 0 ? (d+window_size)*number_words : (d+window_size-1)*number_words;
            cout << "Offset: " << offset << endl;

            int blockSize = 1024;
            int numBlocks = (number_words + blockSize - 1) / blockSize;
            copy_with_offset<<<numBlocks, blockSize>>>(number_words, gpuWords, gpuPair1 + offset, 0);
            copy_with_offset<<<numBlocks, blockSize>>>(number_words, gpuWords, gpuPair2 + offset, d);
        }

        thrust::fill(gpuUniquePair1.begin(), gpuUniquePair1.end(), UINT_MAX);
        thrust::fill(gpuUniquePair2.begin(), gpuUniquePair2.end(), UINT_MAX);
        thrust::fill(gpuCount.begin(), gpuCount.end(), 0);

        cout << "Calculou pairs -----------" << endl;
        // Ordenar vetor gpu Pair
        // --- From raw pointers to device_ptr
        thrust::device_ptr<uint32_t> dev_ptr_pair1 = thrust::device_pointer_cast(gpuPair1);
        thrust::device_ptr<uint32_t> dev_ptr_pair2 = thrust::device_pointer_cast(gpuPair2);
        

        auto begin = thrust::make_zip_iterator(thrust::make_tuple(dev_ptr_pair1, dev_ptr_pair2));
        auto end = thrust::make_zip_iterator(thrust::make_tuple(dev_ptr_pair1 + T, dev_ptr_pair2 + T));

        cout << "Antes do sort" << endl;
        thrust::sort(begin, end, TupleComp());
        cout << "Fez sort ----------" << endl;


        auto begin_unique = thrust::make_zip_iterator(thrust::make_tuple(gpuUniquePair1.begin(), gpuUniquePair2.begin()));

        thrust::unique_copy(begin, end, begin_unique, tupleEqual());

        auto end_unique = thrust::make_zip_iterator(thrust::make_tuple(gpuUniquePair1.end(), gpuUniquePair2.end()));
        thrust::upper_bound(begin, end, begin_unique, end_unique, gpuCount.begin(), TupleComp()); 

        cpuUniquePair1[i] = gpuUniquePair1;
        cpuUniquePair2[i] = gpuUniquePair2;
        cpuCount[i] = gpuCount;

        threads.push_back(std::thread(eliminate_useless_memory, 
                                      i, 
                                      std::ref(cpuUniquePair1[i]), 
                                      std::ref(cpuUniquePair2[i]),
                                      std::ref(cpuCount[i]))
        );
    }

    for(auto &u : threads)
        u.join();

    priority_queue< 
        pair<pair<uint32_t, uint32_t>, pair<uint32_t, int> >
        > pq;
    
    for(i = 0; i < K; i++){
        pq.push({{cpuUniquePair1[i].back(), cpuUniquePair2[i].back()}, {cpuCount[i].back(), i}});
        cpuUniquePair1[i].pop_back();
        cpuUniquePair2[i].pop_back();
        cpuCount[i].pop_back();
    }

    cout << "Calculando resultado final" << endl;
    queue<pair<pair<uint32_t, uint32_t>, uint32_t> > q;
    uint32_t elem1 = 0, elem2;
    int count;
    while(!pq.empty()){
        auto &u = pq.top();
        if(elem1 != u.first.first || elem2 != u.first.second){
            if(elem1 != 0 && elem2 != 0){
                q.push({{elem1, elem2}, count});
            }
            elem1 = u.first.first;
            elem2 = u.first.second;
            count = u.second.first;
        } else {
            count += u.second.first;
        }
        i = u.second.second;
        pq.pop();
        
        if(cpuUniquePair1[i].size() > 0){
            pq.push({{cpuUniquePair1[i].back(), cpuUniquePair2[i].back()}, {cpuCount[i].back(), i}});
            cpuUniquePair1[i].pop_back();
            cpuUniquePair2[i].pop_back();
            cpuCount[i].pop_back();
        }
    }
    cudaEventRecord(stop);

    cout << "Imprimir resultado" << endl;
    fid = fopen("result.txt","w");
    while(!q.empty()){
        auto &u = q.front();
        fprintf(fid, "%u %u %u\n", u.first.first, u.first.second, u.second);
        q.pop();
    }    
    
    // Imprimir resultado
    
    cudaFree(gpuWords);
    cudaFree(gpuPair1);
    cudaFree(gpuPair2);
    
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    total_time += milliseconds/1000;

    cout << "Tempo total de execução sem I/O: " << total_time << endl;

    return 0;   
}

