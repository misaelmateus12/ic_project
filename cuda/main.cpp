#include<bits/stdc++.h>
#include "fnv1a.h"
using namespace std;
hash<string> hasher;
const long long P = 1e9 + 7;
int word_hash[P];

const uint8_t MAX_WORD_LENGTH = 30;
char *GetNextWord (char *text, char *token)
{
    bool quote_found = false;
    bool hyphen_found = false;

    uint8_t char_index = 0;


    while (*text)
    {
        char c = *text++;

        if (isalpha (c))
        {
            c = static_cast<char>(tolower (c));

            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }
        }
        else if ((c == '\'') && (!quote_found) && (char_index > 0))
        {
            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }

            quote_found = true;
        }
        else if ((c == '-') && (!hyphen_found) && (char_index > 0))
        {
            if (char_index < MAX_WORD_LENGTH)
            {
                token[char_index++] = c;
            }

            hyphen_found = true;
        }
        else if (char_index > 0)
        {
            //
            // Normaliza o índice para evitar que a string passe do limite do buffer.
            //
            char_index = (uint8_t) ((char_index > MAX_WORD_LENGTH - 1) ? MAX_WORD_LENGTH - 1 : char_index);

            //
            // Descarta o apóstrofo ou o hífen se este estiver isolado no final do token.
            //
            // Ex.: 'String' -> String'
            //      'Window' -> Window'
            //
            if ((token[char_index - 1] == '\'') || (token[char_index - 1] == '-'))
            {
                --char_index;
            }

            break;
        } else break;
    }

    char_index = min(char_index, MAX_WORD_LENGTH);
    token[char_index] = '\0';

    return (*text) ? text : nullptr;
}


int main(){
    ios::sync_with_stdio(0);
    cin.tie(0);
    char word[50];
    char aux[50];
    
    int i = 0, cnt = 0;
    
    ofstream out;
    out.open("vocabulary.txt", ofstream::out);
    
    while(cin >> word){
        if(word[0] == '\0') continue;

        GetNextWord(word, aux);
        if(strlen(aux) <= 2) continue;
        word_hash[i++] = fnv1a(aux);
          
        // ----- LOG -------
        cnt++;
        if(cnt == 2000000){
            cout << i << endl;
            cnt = 0;
        }
        // -----------------
    }


    out.close();
    return 0;   
}

