#include <string.h>
#include <iostream>

char specialWords[][10] = {"inning", "outing", "canning", "herring", "earring", "proceed","exceed", "succeed"};
char step2Words[][2][10] = {{"ational", "ate"},
                  {"tional", "tion"},
                  {"enci", "ence"},
                  {"anci", "ance"},
                  {"abli", "able"},
                  {"entli", "ent"},
                  {"izer", "ize"},
                  {"ization", "ize"},
                  {"ation", "ate"},
                  {"ator", "ate"},
                  {"alism", "al"},
                  {"aliti", "al"},
                  {"alli", "al"},
                  {"fulness", "ful"},
                  {"ousli", "ous"},
                  {"ousness", "ous"},
                  {"iveness", "ive"},
                  {"iviti", "ive"},
                  {"biliti", "ble"},
                  {"bli", "ble"},
                  {"fulli", "ful"},
                  {"lessli", "less"}};

char step3Words[][2][10] = {{"ational", "ate"},
                  {"tional", "tion"},
                  {"alize", "al"},
                  {"icate", "ic"},
                  {"iciti", "ic"},
                  {"ical", "ic"},
                  {"ful", ""},
                  {"ness", ""}};

char step4Words[][2][8] = {{"al", ""},
                  {"ance", ""},
                  {"ence", ""},
                  {"er", ""},
                  {"ic", ""},
                  {"able", ""},
                  {"ible", ""},
                  {"ant", ""},
                  {"ement", ""},
                  {"ment", ""},
                  {"ism", ""},
                  {"ate", ""},
                  {"iti", ""},
                  {"ous", ""},
                  {"ive", ""},
                  {"ize", ""}};
bool isVowelY(char ch)
{
    return ch == 'e' || ch == 'a' || ch == 'i' || ch == 'o' || ch == 'u'
           || ch == 'y';
}

bool isVowel(char c)
{
    return c == 'e' || c == 'a' || c == 'i' || c == 'o' || c == 'u';
}

bool replaceIfExists(
    char *word, const char* suffix,
    const char* replacement, size_t start, int tWord, int tSuffix = -1)
{
    if(tSuffix == -1) tSuffix = strlen(suffix);
    if (tSuffix > tWord)
        return false;

    size_t idx = tWord - tSuffix;
    if (idx < start)
        return false;

    auto diff = static_cast<std::string::iterator::difference_type>(idx);
    if (strcmp(word + idx, suffix) == 0)
    {
        strcpy(word + idx, replacement);
        return true;
    }
    return false;
}

bool isShort(char *word, int d, int t)
{
    size_t size = t - d;

    if (size >= 3)
    {
        if (!isVowelY(word[size - 3]) && isVowelY(word[size - 2])
            && !isVowelY(word[size - 1]) && word[size - 1] != 'w'
            && word[size - 1] != 'x' && word[size - 1] != 'Y')
            return true;
    }
    return size == 2 && isVowelY(word[0]) && !isVowelY(word[1]);
}

bool endsWith(char *word, const char * str, int t1, int t2 = -1)
{
   if(t2 = -1) t2 = strlen(str);
	  if (t1 < t2)
        return false;

    return strcmp(word + (t1 - t2), str) == 0;
}

bool endsInDouble(char* word, int t)
{	
    if (t >= 2)
    {
        char a = word[t - 1];
        char b = word[t - 2];

        if (a == b)
            return a == 'b' || a == 'd' || a == 'f' || a == 'g' || a == 'm'
                   || a == 'n' || a == 'p' || a == 'r' || a == 't';
    }

    return false;
}

bool isValidLIEnding(char ch)
{
    return ch == 'c' || ch == 'd' || ch == 'e' || ch == 'g' || ch == 'h'
           || ch == 'k' || ch == 'm' || ch == 'n' || ch == 'r' || ch == 't';
}

bool containsVowel(char *word, size_t start, size_t end)
{
   int t = strlen(word);
    if (end <= t)
    {
        for (size_t i = start; i < end; ++i)
            if (isVowelY(word[i]))
                return true;
    }
    return false;
}

int firstNonVowelAfterVowel(char *word, size_t start, int t)
{
  
    for (size_t i = start; i != 0 && i < t; ++i)
    {
        if (!isVowelY(word[i]) && isVowelY(word[i - 1]))
            return i + 1;
    }

    return t;
}

int getStartR1(char *word, int t)
{
    // special cases
    if (t >= 5 && word[0] == 'g' && word[1] == 'e' && word[2] == 'n'
        && word[3] == 'e' && word[4] == 'r')
        return 5;
    if (t >= 6 && word[0] == 'c' && word[1] == 'o' && word[2] == 'm'
        && word[3] == 'm' && word[4] == 'u' && word[5] == 'n')
        return 6;
    if (t >= 5 && word[0] == 'a' && word[1] == 'r' && word[2] == 's'
        && word[3] == 'e' && word[4] == 'n')
        return 5;

    // general case
    return firstNonVowelAfterVowel(word, 1, t);
}

int getStartR2(char * word, size_t startR1, int t)
{
  
    if (startR1 == t)
        return startR1;

    return firstNonVowelAfterVowel(word, startR1 + 1, t);
}

int special(char *c){
	switch(c[0]){
		case 'e':
			if(strcmp(c, "early") == 0){
				strcpy(c, "earli");
				return true;
			}
			return strcmp(c, specialWords[4]) == 0 || strcmp(c, specialWords[6]) == 0;
		case 'i':
			if(strcmp(c, "idly") == 0){
				strcpy(c, "idl");
				return true;
			}
			return strcmp(c, specialWords[0]) == 0;
		case 'o':
			if(strcmp(c, "only") == 0){
				strcpy(c, "onli");
				return true;
			}
			return strcmp(c, specialWords[1]) == 0;
		case 'c':
			return strcmp(c, specialWords[2]) == 0;
		case 'h':
			return strcmp(c, specialWords[3]) == 0;
		case 'p':
			return strcmp(c, specialWords[5]) == 0;
		case 's':
			if(strcmp(c, "skies") == 0){
				strcpy(c, "sky");
				return true;
			}
			if(strcmp(c, "skis") == 0){
				strcpy(c, "ski");
				return true;
			}
			if(strcmp(c, "singly") == 0){
				strcpy(c, "singl");
				return true;
			}
			return strcmp(c, specialWords[7]) == 0;
		case 'u':
			if(strcmp(c, "ugly") == 0){
				strcpy(c, "ugli");
				return true;
			}
			return false;
		case 'd':
			if(strcmp(c, "dying") == 0){
				strcpy(c, "die");
				return true;
			}
			return false;
		case 'l':
			if(strcmp(c, "lying") == 0){
				strcpy(c, "lie");
				return true;
			}
			return false;
		case 't':
			if(strcmp(c, "tying") == 0){
				strcpy(c, "tie");
				return true;
			}
			return false;
		case 'g':
			if(strcmp(c, "gently") == 0){
				strcpy(c, "gentl");
				return true;
			}
		default:
			return false;
	}
}


void changeY(char *word, int t){
    if (word[0] == 'y')
        word[0] = 'Y';
    for (int i = 1; i < t; ++i)
    {
        if (word[i] == 'y' && isVowel(word[i - 1]))
            word[i++] = 'Y'; // skip next iteration
    }
}

int step0(char* word, int t)
{
    // short circuit the longest suffix
    if(replaceIfExists(word, "'s'", "", 0, t, 3) || replaceIfExists(word, "'s", "", 0, t, 2)
        || replaceIfExists(word, "'", "", 0, t, 1) ) return strlen(word);
    return t;
}

bool step1A(char * word, int t)
{
    if (!replaceIfExists(word, "sses", "ss", 0, t, 4))
    {
    	
        if (endsWith(word, "ied", t, 3) || endsWith(word, "ies", t, 3))
        {
            // if preceded by only one letter
            
            if (t <= 4)
                word[t-1] = '\0', t--;
            else
            {
                word[t-1] = '\0', t--;
                word[t-1] = '\0', t--;
            }
        }
        else if (endsWith(word, "s", t, 1) && !endsWith(word, "us", t, 2)
                 && !endsWith(word, "ss", t, 2))
        {
            if (t > 2 && containsVowel(word, 0, t - 2))
                word[t-1] = '\0', t--;
        }
    }
    else t = strlen(word);
    // special case after step 1a
    return (t == 6 || t == 7)
           && (strcmp(word, "inning") == 0 || strcmp(word, "outing") == 0 || strcmp(word, "canning") == 0
               || strcmp(word, "herring") == 0 || strcmp(word, "earring") == 0 || strcmp(word, "proceed") == 0
               || strcmp(word, "exceed") == 0 || strcmp(word, "succeed") == 0);
}


void step1B(char * word, size_t startR1)
{
   int t = strlen(word);
    bool exists = endsWith(word, "eedly", t, 5) || endsWith(word, "eed", t, 3);
    
     //   std::cout << "Step 1B: " << word << " " << size << " " << exists << "\n";
        
    if (exists){ // look only in startR1 now
      if(replaceIfExists(word, "eedly", "ee", startR1, t, 5) || replaceIfExists(word, "eed", "ee", startR1, t, 3) )
        t = strlen(word);
    }
    else
    {
        int size = t;
        bool deleted = (containsVowel(word, 0, size - 2)
                        && replaceIfExists(word, "ed", "", 0, size, 2))
                       || (containsVowel(word, 0, size - 4)
                           && replaceIfExists(word, "edly", "", 0, size, 4))
                       || (containsVowel(word, 0, size - 3)
                           && replaceIfExists(word, "ing", "", 0, size, 3))
                       || (containsVowel(word, 0, size - 5)
                           && replaceIfExists(word, "ingly", "", 0, size, 5));
        if(deleted)
          t = strlen(word);
        if (deleted && (endsWith(word, "at", t, 2) || endsWith(word, "bl", t, 2)
                        || endsWith(word, "iz", t, 2))){
            word[t] = 'e', word[t+1] = '\0', t++;
    	 }
        else if (deleted && endsInDouble(word, t)){
            word[--t] = '\0';
        }
        else if (deleted && startR1 == t && isShort(word, 0, t))
            word[t++] = 'e', word[t] = '\0';	 
    }
}

void step1C(char* word)
{
    size_t size = strlen(word);
    if (size > 2 && (word[size - 1] == 'y' || word[size - 1] == 'Y'))
        if (!isVowel(word[size - 2]))
            word[size - 1] = 'i';
}


void step2(char * word, size_t startR1)
{
    int t = strlen(word);
    for (int i = 0; i < 22; i++)
        if (replaceIfExists(word, step2Words[i][0], step2Words[i][1], startR1, t))
            return;

    if (!replaceIfExists(word, "logi", "log", startR1 - 1, t, 4))
    {
        // make sure we choose the longest suffix
        if (endsWith(word, "li", t, 2) && !endsWith(word, "abli", t, 4)
            && !endsWith(word, "entli", t, 5) && !endsWith(word, "aliti", t, 5)
            && !endsWith(word, "alli", t, 4) && !endsWith(word, "ousli", t, 5)
            && !endsWith(word, "bli", t, 3) && !endsWith(word, "fulli", t, 5)
            && !endsWith(word, "lessli", t, 6))
            if (t > 3 && t - 2 >= startR1
                && isValidLIEnding(word[t - 3]))
            {
                word[--t] = '\0';
                word[--t] = '\0';
            }
    }
}

void step3(char *word, size_t startR1, size_t startR2)
{
    int t = strlen(word);
    for(int i = 0; i < 8; i++)
        if (replaceIfExists(word, step3Words[i][0], step3Words[i][1], startR1, t))
            return;

    replaceIfExists(word, "ative", "", startR2, t);
}

void step4(char * word, size_t startR2)
{
    int t = strlen(word);
    for(int i = 0; i < 16; i++)
        if (replaceIfExists(word, step4Words[i][0], step4Words[i][1], startR2, t))
            return;

    // make sure we only choose the longest suffix
    if (!endsWith(word, "ement", t, 5) && !endsWith(word, "ment", t, 4))
        if (replaceIfExists(word, "ent", "", startR2, t, 3))
            return;

    // short circuit
    replaceIfExists(word, "sion", "s", startR2 - 1, t, 4)
        || replaceIfExists(word, "tion", "t", startR2 - 1, t, 4);
}

void step5(char *word, size_t startR1,
                                     size_t startR2)
{
    size_t size = strlen(word);
    if (word[size - 1] == 'e')
    {
        if (size - 1 >= startR2)
            word[--size] = '\0';
        else if (size - 1 >= startR1 && !isShort(word, 1, size))	
            word[--size] = '\0';
    }
    else if (word[size - 1] == 'l')
    {
        if (size - 1 >= startR2 && word[size - 2] == 'l')
            word[--size] = '\0';
    }
}
void stemming(char *word){
	int t = strlen(word);
	if(t <= 2) return ;

	if(t > 35) t = 35, word[t] = '\0';
	if(word[0] == '\''){
		for(int i = 0; true; i++){
			word[i] = word[i+1];
      if(word[i] == '\0')
        break;
    }
		t--;
	}

	if(special(word)) return ;
  changeY(word, t);

  size_t startR1 = getStartR1(word, t);
  size_t startR2 = getStartR2(word, startR1, t);
  
  t = step0(word, t);

  if (step1A(word, t)){
    for(int i = 0; word[i] != '\0'; i++)
      if(word[i] == 'Y') word[i] = 'y';
    return ;
  }

  step1B(word, startR1);
  step1C(word);
  step2(word, startR1);
  step3(word, startR1, startR2);
  step4(word, startR2);
  step5(word, startR1, startR2);

  for(int i = 0; word[i] != '\0'; i++)
      if(word[i] == 'Y') word[i] = 'y';

}	
